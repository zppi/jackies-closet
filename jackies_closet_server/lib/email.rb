module Email
  VALID_EMAIL_REGEX = /\A[\w+\-.]+@[a-z\d\-]+(\.[a-z\d\-]+)*\.[a-z]+\z/i

  def is_a_valid_email?(email)
    (email =~ VALID_EMAIL_REGEX)
  end

end