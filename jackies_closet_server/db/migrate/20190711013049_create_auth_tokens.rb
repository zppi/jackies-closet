class CreateAuthTokens < ActiveRecord::Migration[6.0]
  def change
    create_table :auth_tokens do |t|
      t.integer :user_id
      t.uuid :session_token, default: 'uuid_generate_v4()'

      t.timestamps
    end
  end
end
