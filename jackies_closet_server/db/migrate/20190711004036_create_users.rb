class CreateUsers < ActiveRecord::Migration[6.0]
  def change
    create_table :users do |t|
      t.string :name
      t.string :email
      t.uuid :uuid, default: 'uuid_generate_v4()'

      t.timestamps
    end
  end
end
