class AddIsSharedSetting < ActiveRecord::Migration[6.0]
  def change
    add_column :items, :is_shared, :boolean, default: true
  end
end
