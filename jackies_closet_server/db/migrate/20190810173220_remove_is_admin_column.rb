class RemoveIsAdminColumn < ActiveRecord::Migration[6.0]
  def change
    remove_column :groups_users, :is_admin
  end
end
