class ChangeSessionTokenType < ActiveRecord::Migration[6.0]
  def change
    remove_column :auth_tokens, :session_token
    add_column :auth_tokens, :session_token, :string
  end
end
