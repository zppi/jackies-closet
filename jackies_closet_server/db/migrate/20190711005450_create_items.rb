class CreateItems < ActiveRecord::Migration[6.0]
  def change
    create_table :items do |t|
      t.string :name
      t.string :email
      t.uuid :uuid, default: 'uuid_generate_v4()'
      t.integer :user_id
      t.integer :group_id

      t.timestamps
    end
  end
end
