class ChangeVerificationTokenString < ActiveRecord::Migration[6.0]
  def change
    remove_column :auth_tokens, :verification_token
    add_column :auth_tokens, :verification_token, :string
  end
end
