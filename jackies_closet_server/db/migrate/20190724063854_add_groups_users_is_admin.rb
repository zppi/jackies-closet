class AddGroupsUsersIsAdmin < ActiveRecord::Migration[6.0]
  def change
    add_column :groups_users, :is_admin, :boolean, default: 'false'
  end
end
