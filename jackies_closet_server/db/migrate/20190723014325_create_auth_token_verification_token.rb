class CreateAuthTokenVerificationToken < ActiveRecord::Migration[6.0]
  def change
    add_column :auth_tokens, :verification_token, :uuid, default: 'uuid_generate_v4()'
  end
end
