class AddIndexes < ActiveRecord::Migration[6.0]
  def change
    add_index :groups, :uuid
    add_index :users, :uuid
    add_index :items, :uuid
  end
end
