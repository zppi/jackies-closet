class AddChildcareCategory < ActiveRecord::Migration[6.0]
  def change
    change_column :groups, :categories, :string, array: true, default: ['kitchen', 'childcare', 'outdoor & camping', 'festivity', 'emergency preparedness', 'tools']
  end
end
