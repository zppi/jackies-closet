class ChangeAdminIds < ActiveRecord::Migration[6.0]
  def change
    add_column :groups, :admin_ids, :string, array: true, default: []
  end
end
