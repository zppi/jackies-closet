class AuthToken < ApplicationRecord
  validates :session_token, presence: true

  belongs_to :user, required: false
end
