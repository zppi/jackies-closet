class Item < ApplicationRecord
  validates :name, presence: true, length: { maximum: 50 }
  validates :user_id, presence: true, numericality: { only_integer: true }
  validates :group_id, presence: true, numericality: { only_integer: true }

  belongs_to :group
  belongs_to :user
end
