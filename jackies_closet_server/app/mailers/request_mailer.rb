class RequestMailer < ApplicationMailer
  default from: "Jackie's Closet <simplerequests@jackiescloset.app>"

  def request_something(sender_user, item_name, message, group)
    @groupName = group.name
    @message = message

    emails = group.users.collect(&:email).join(",")
    mail(
      to: emails,
      subject: "#{sender_user.name} in #{@groupName} is looking for #{item_name}",
      reply_to: sender_user.email
    )
  end

end
