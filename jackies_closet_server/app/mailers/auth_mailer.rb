class AuthMailer < ApplicationMailer
  default from: "Jackie's Closet <noreply@jackiescloset.app>"

  def magic_link(callback_host, auth_token, email)
    @url = callback_host + "/verify/" + email + "/" + auth_token.verification_token

    mail(
      to: email,
      subject: 'Sign into Jackie\'s Closet!'
    )
  end

  def invitation(callback_host, auth_token, email, sender_name, group_name)
    @url = callback_host + "/verify/" + email + "/" + auth_token.verification_token

    mail(
      to: email,
      subject: sender_name + ' invited you to ' + group_name + '!'
    )
  end

end
