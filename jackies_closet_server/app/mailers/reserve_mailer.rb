class ReserveMailer < ApplicationMailer
  default from: "Jackie's Closet <simplerequests@jackiescloset.app>"

  def request_reservation(recipient_user, sender_user, item, start_date, end_date, note)

    some_variable = "some thing else"

    @recipientName = recipient_user.name
    @senderName = sender_user.name
    @groupName = item.group.name
    @itemName = item.name

    @dateRange = "on #{start_date.strftime("%A, %B %e")}."
    if end_date.present?
      @dateRange = " from #{start_date.strftime("%A, %B %e")} to #{end_date.strftime("%A, %B %e")}."
    end

    @note = note

    mail(
      to: recipient_user.email,
      subject: "#{sender_user.name} has requested to use #{item.name}",
      reply_to: sender_user.email
    )
  end

end
