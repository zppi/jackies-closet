module Types
  class ItemType < BaseObject
    field :name, String, null: false
    field :id, String, null: false
    field :user, UserType, null: false
    field :group, GroupType, null: false
    field :category, String, null: true
    field :is_shared, Boolean, null: false
  end
end