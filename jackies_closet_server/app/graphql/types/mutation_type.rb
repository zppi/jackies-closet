module Types
  class MutationType < Types::BaseObject
    field :send_login_email, mutation: Mutations::SendLoginEmail
    field :verify_token, mutation: Mutations::VerifyToken
    field :create_group, mutation: Mutations::CreateGroup
    field :edit_group, mutation: Mutations::EditGroup
    field :add_to_group, mutation: Mutations::AddToGroup
    field :create_item, mutation: Mutations::CreateItem
    field :delete_item, mutation: Mutations::DeleteItem
    field :edit_item, mutation: Mutations::EditItem
    field :sign_out, mutation: Mutations::SignOut
    field :request_reservation, mutation: Mutations::RequestReservation
    field :edit_profile, mutation: Mutations::EditProfile
    field :request_something, mutation: Mutations::RequestSomething
    field :remove_owner, mutation: Mutations::RemoveOwner
    field :edit_admin, mutation: Mutations::EditAdmin
  end
end
