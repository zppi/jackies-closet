module Types
  class QueryType < Types::BaseObject

    field :initial_data, UserType, null: true
    field :user, UserType, null: true do
      argument :id, String, required: true
    end
    field :group, GroupType, null: true do
      argument :id, String, required: true
    end
    field :item, ItemType, null: true do
      argument :id, String, required: true
    end
    field :groups_to_join, [GroupType], null: false

    def initial_data
      if !current_user
        return GraphQL::ExecutionError.new("Log in to view your groups")
      end
      current_user
    end

    def user(id:)
      if !current_user
        return GraphQL::ExecutionError.new("Log in to view a user")
      end
      User.find_by(uuid: id)
    end

    def group(id:)
      if !current_user
        return GraphQL::ExecutionError.new("Log in to view a group")
      end
      Group.find_by(uuid: id)
    end

    def item(id:)
      if !current_user
        return GraphQL::ExecutionError.new("Log in to view an item")
      end
      Item.find_by(uuid: id)
    end

    def groups_to_join
      if !current_user
        return GraphQL::ExecutionError.new("Log in to view groups to join")
      end
      Group.all.select { |anyGroup| (current_user.groups.select { |userGroup| userGroup.id == anyGroup.id}).empty? }.sort_by{ |group| group.name.downcase}
    end
  end
end
