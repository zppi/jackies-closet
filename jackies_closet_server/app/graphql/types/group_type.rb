module Types
  class GroupType < BaseObject
    field :name, String, null: false
    field :id, String, null: false
    field :users, [UserType], null: false
    field :items, [ItemType], null: false
    field :admin_ids, [String], null: false
    field :categories, [String], null: false

    def users
      if @object.admin_ids.include?(current_user.uuid)
        return @object.users
      end
      return []
    end

    def items
      if @object.admin_ids.include?(current_user.uuid)
        return @object.items
      end
      return @object.items.select{|item| item.is_shared || item.user_id === current_user.id}
    end

  end
end