module Types
  class GroupUsersType < BaseObject
    field :group, GroupType, null: false
    field :users, [UserType], null: false
  end
end