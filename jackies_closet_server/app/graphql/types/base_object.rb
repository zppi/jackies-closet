module Types
  class BaseObject < GraphQL::Schema::Object
    def id
      object.uuid
    end

    def current_user
      context[:current_user]
    end
  end
end
