module Types
  class UserType < BaseObject
    field :name, String, null: true
    field :email, String, null: false
    field :id, String, null: false
    field :groups, [GroupType], null: false
    field :items, [ItemType], null: false

    def groups
      @object.groups.sort_by{ |group| group.name.downcase}
    end
  end
end