class Mutations::AddToGroup < Mutations::BaseMutation

  argument :id, String, required: true
  argument :emails, [String], required: true

  type Types::GroupUsersType

  def resolve(id: nil, emails: [])
    if !current_user
      return GraphQL::ExecutionError.new("Log in to add users to a group")
    end

    @group = Group.find_by(uuid: id)

    if !@group
      return GraphQL::ExecutionError.new("That group does not exist")
    end

    usersToAdd = []

    callback_host = 'https://www.jackiescloset.app'
    if context[:request].referrer.include?('localhost')
      callback_host = 'http://localhost:3000'
    end

    emails.each { |email|
      if !is_a_valid_email?(email)
        return GraphQL::ExecutionError.new("The email #{email} is invalid")
      end
      @user = User.find_or_create_by!(email: email)
      @user.reload
      if !@group.users.include?(@user)
        usersToAdd.push(@user)
        @auth_token = AuthToken.create!(
          user: @user,
          verification_token: SecureRandom.urlsafe_base64(30),
          session_token: SecureRandom.urlsafe_base64(30)
        )
        AuthMailer.invitation(
          callback_host,
          @auth_token,
          email.downcase,
          current_user.name,
          @group.name
        ).deliver
      end
    }

    usersToAdd.each { |user|
      @group.users.push(user)
    }

    return {group: @group, users: usersToAdd}
  end


  VALID_EMAIL_REGEX = /\A[\w+\-.]+@[a-z\d\-]+(\.[a-z\d\-]+)*\.[a-z]+\z/i

  def is_a_valid_email?(email)
    (email =~ VALID_EMAIL_REGEX)
  end

end
