class Mutations::RemoveOwner < Mutations::BaseMutation

  argument :group_id, String, required: true
  argument :user_id, String, required: true

  type Types::UserType

  def resolve(group_id: nil, user_id: nil)
    if !current_user
      return GraphQL::ExecutionError.new("Log in to add users to a group")
    end

    @group = Group.find_by(uuid: group_id)

    if !@group
      return GraphQL::ExecutionError.new("That group does not exist")
    end

    is_admin = @group.admin_ids.include?(current_user.uuid)

    if !is_admin
      return GraphQL::ExecutionError.new("You are not an admin in this group")
    end

    @user = User.find_by(uuid: user_id)

    if !@user
      return GraphQL::ExecutionError.new("No user was found")
    end

    if !@user.groups.include?(@group)
      return GraphQL::ExecutionError.new("This is not a member of your group")
    end

    @user.items.each { |item| 
      if item.group_id == @group.id
        item.destroy
      end
    }

    @user.groups.delete(@group)
    @user.save
    return @user.reload
  end

end
