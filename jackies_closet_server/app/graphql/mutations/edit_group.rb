class Mutations::EditGroup < Mutations::BaseMutation

  argument :id, String, required: true
  argument :name, String, required: false

  type Types::GroupType

  def resolve(id: nil, name: nil)
    if !current_user
      return GraphQL::ExecutionError.new("Log in to edit a group")
    end

    @group = Group.find_by(uuid: id)
    if !@group
      return GraphQL::ExecutionError.new("That group does not exist")
    end
    
    if name.present?
      if name.length < 5
        return GraphQL::ExecutionError.new("That name is too short")
      end
      @previousGroup = Group.find_by(name: name)
      if @previousGroup
        return GraphQL::ExecutionError.new("That name is already taken")
      end
      @group.name = name
    end

    @group.save
    return @group.reload
  end

end
