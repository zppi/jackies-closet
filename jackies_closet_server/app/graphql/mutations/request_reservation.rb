class Mutations::RequestReservation < Mutations::BaseMutation

  argument :item_id, String, required: true
  argument :start_date, Integer, required: true
  argument :end_date, Integer, required: false
  argument :note, String, required: false

  field :successful, String, null: false

  def resolve(item_id: nil, start_date: nil, end_date: nil, note: nil)
    @item = Item.find_by(uuid: item_id)

    if !@item
      return GraphQL::ExecutionError.new("No item matches that id");
    end

    if !start_date.present?
      return GraphQL::ExecutionError.new("You need to give the date you'd like to borrow this item");
    end

    @start_date = Time.at(start_date)

    @end_date = nil

    if end_date
      @end_date = Time.at(end_date)
    end

    ReserveMailer.request_reservation(
      @item.user,
      current_user,
      @item,
      @start_date,
      @end_date,
      note
    ).deliver

    { successful: "true"}
  end

end
