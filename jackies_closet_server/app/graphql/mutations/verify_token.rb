class Mutations::VerifyToken < Mutations::BaseMutation

  argument :verification_token, String, required: true

  field :session_token, String, null: false

  def resolve(verification_token: nil)

    if !verification_token.present?
      return GraphQL::ExecutionError.new("You must provide a verification token.")
    end

    @authToken = AuthToken.find_by(verification_token: verification_token)

    if !@authToken
      return GraphQL::ExecutionError.new("It looks like you have an invalid verification token.")
    end

    @authToken.update_attribute(:verification_token, nil)

    return @authToken
  end

end
