class Mutations::SignOut < Mutations::BaseMutation

  argument :session_token, String, required: true

  field :confirmation, String, null: false

  def resolve(session_token: nil)

    @authToken = AuthToken.find_by(session_token: session_token)

    if !@authToken
      return {confirmation: "false"}
    end

    @authToken.destroy()
    return {confirmation: "true"}
  end
end
