class Mutations::RequestSomething < Mutations::BaseMutation

  argument :item_name, String, required: true
  argument :message, String, required: false
  argument :group_ids, [String], required: true

  field :successful, String, null: false

  def resolve(item_name: nil, message: nil, group_ids: nil)
    if !current_user
      return GraphQL::ExecutionError.new("Log in to create a request")
    end

    if !item_name.present?
      return GraphQL::ExecutionError.new("You need to give the name of the item you'd like to borrow");
    end

    if !group_ids.present?
      return GraphQL::ExecutionError.new("You need to provide the groups you'd like to send this request to");
    end

    groups = []
    group_ids.each { |id| 
      groups.push(Group.find_by(uuid: id))
    }

    informedRecipients = []

    groups.each { |group|
      RequestMailer.request_something(
        current_user,
        item_name,
        message,
        group
      ).deliver
    }
    
    { successful: "true"}
  end

end
