class Mutations::CreateItem < Mutations::BaseMutation

  argument :name, String, required: true
  argument :group_id, String, required: true
  argument :category, String, required: false
  argument :is_shared, Boolean, required: false

  type Types::ItemType

  def resolve(name: nil, group_id: nil, category: nil, is_shared: nil)
    if !current_user
      return GraphQL::ExecutionError.new("Log in to create an item")
    end

    if !name || name.length < 5
      return GraphQL::ExecutionError.new("That name is too short")
    end

    @group = Group.find_by(uuid: group_id)

    if !@group
      return GraphQL::ExecutionError.new("That group does not exist")
    end
    items = @group.items

    if !(@group.items.select { |item| item.name == name }.empty?)
      return GraphQL::ExecutionError.new("That name is already taken")
    end

    @item = Item.create!(
      name: name,
      user: current_user,
      group: @group,
      category: category,
      is_shared: is_shared
    )
    return @item.reload
      
  end

end
