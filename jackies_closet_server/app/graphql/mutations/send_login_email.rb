class Mutations::SendLoginEmail < Mutations::BaseMutation

  argument :email, String, required: true

  field :email, String, null: false

  def resolve(email: nil)
    email = email.gsub(/\s+/, "").downcase
    if !is_a_valid_email?(email)
      return GraphQL::ExecutionError.new("That email doesn't seem valid, please try another")
    end

    @user = User.find_or_create_by!(email: email)

    @auth_token = AuthToken.create!(
      user: @user,
      verification_token: SecureRandom.urlsafe_base64(30),
      session_token: SecureRandom.urlsafe_base64(30)
    )
    callback_host = 'https://www.jackiescloset.app'
    if context[:request].referrer.include?('localhost')
      callback_host = 'http://localhost:3000'
    end

    AuthMailer.magic_link(
      callback_host,
      @auth_token,
      email.downcase
    ).deliver
    return { email: email }
  end

  VALID_EMAIL_REGEX = /\A[\w+\-.]+@[a-z\d\-]+(\.[a-z\d\-]+)*\.[a-z]+\z/i

  def is_a_valid_email?(email)
    (email =~ VALID_EMAIL_REGEX)
  end

end
