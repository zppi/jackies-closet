class Mutations::DeleteItem < Mutations::BaseMutation

  argument :id, String, required: true

  type Types::ItemType

  def resolve(id: nil)
    if !current_user
      return GraphQL::ExecutionError.new("Log in to delete an item")
    end

    @item = Item.find_by(uuid: id)

    if !@item
      return GraphQL::ExecutionError.new("That item does not exist")
    end

    if current_user.id != @item.user.id && !@item.group.admin_ids.include?(current_user.uuid)
      return GraphQL::ExecutionError.new("You do not own this item")
    end

    @item.destroy

    return @item
  end

end
