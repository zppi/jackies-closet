class Mutations::CreateGroup < Mutations::BaseMutation

  argument :name, String, required: true

  type Types::GroupType

  def resolve(name: nil)
    if !current_user
      return GraphQL::ExecutionError.new("Log in to create a group")
    end

    if !name || name.length < 5
      return GraphQL::ExecutionError.new("That name is too short")
    end

    @group = Group.find_by(name: name)

    if !@group
      @group = Group.create!(
        name: name
      )
      @group.users.push(current_user)
      @group.admin_ids.push(current_user.uuid)
      @group.save
      return @group.reload
    else
      return GraphQL::ExecutionError.new("That name is already taken")
    end
  end

end
