class Mutations::EditProfile < Mutations::BaseMutation

  argument :name, String, required: false

  field :name, String, null: false

  def resolve(name: nil)
    if !current_user
      return GraphQL::ExecutionError.new("Log in to edit your profile.")
    end

    if name.length < 5
      return GraphQL::ExecutionError.new("Your name must be longer than 5 characters.")
    end

    current_user.update_attribute(:name, name)

    return current_user.reload
  end
end
