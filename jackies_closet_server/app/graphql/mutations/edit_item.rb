class Mutations::EditItem < Mutations::BaseMutation

  argument :id, String, required: true
  argument :name, String, required: false
  argument :category, String, required: false
  argument :is_shared, Boolean, required: false

  type Types::ItemType

  def resolve(id: nil, name: nil, category: nil, is_shared: nil)
    if !current_user
      return GraphQL::ExecutionError.new("Log in to delete an item")
    end

    @item = Item.find_by(uuid: id)

    if !@item
      return GraphQL::ExecutionError.new("That item does not exist")
    end

    if name
      if name.length < 5
        return GraphQL::ExecutionError.new("That name is too short")
      else
        @item.name = name
      end
    end

    if category
      @item.category = category
    end

    if is_shared != nil
      @item.is_shared = is_shared
    end

    if current_user.id != @item.user.id
      return GraphQL::ExecutionError.new("You do not own this item")
    end

    @item.save
    return @item.reload
  end
end
