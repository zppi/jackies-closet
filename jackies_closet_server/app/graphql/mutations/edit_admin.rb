class Mutations::EditAdmin < Mutations::BaseMutation

  argument :user_id, String, required: true
  argument :group_id, String, required: false
  argument :should_be_admin, Boolean, required: false

  type Types::GroupType

  def resolve(user_id: nil, group_id: nil, should_be_admin: nil)
    if !current_user
      return GraphQL::ExecutionError.new("Log in edit an admin")
    end

    @user = User.find_by(uuid: user_id)
    if !@user
      return GraphQL::ExecutionError.new("That user does not exist")
    end

    @group = Group.find_by(uuid: group_id)
    if !@group
      return GraphQL::ExecutionError.new("That group does not exist")
    end

    if should_be_admin
      if !@group.admin_ids.include?(user_id)
        @group.admin_ids.push(user_id)
      else
        return GraphQL::ExecutionError.new(@user.name + " is already an admin in " + @group.name)
      end
    else
      if @group.admin_ids.include?(user_id)
        @group.admin_ids.delete(user_id)
      else
        return GraphQL::ExecutionError.new(@user.name + " is not an admin in " + @group.name)
      end
    end


    @group.save
    return @group.reload
  end
end
