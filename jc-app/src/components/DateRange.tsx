import "date-fns";
import React from "react";
import { MdMoreVert } from "react-icons/md";

import { DatePicker } from "react-md";
import Checkbox from "@material-ui/core/Checkbox";

import "./styles/DateRange.scss";

interface Props {
  startDate: Date;
  endDate: Date;
  onStartDateChange: any;
  onEndDateChange: any;
  isRange: boolean;
  onIsRangeChange: any;
  isValid: boolean;
}

class DateRange extends React.Component<Props> {
  render() {
    const {
      startDate,
      endDate,
      onStartDateChange,
      onEndDateChange,
      isRange,
      onIsRangeChange,
      isValid
    } = this.props;
    return (
      <div className="date-picker-container">
        <div className="select-date-range">
          <Checkbox
            checked={isRange}
            onChange={(event: any) => onIsRangeChange(event)}
            color="primary"
          />
          <span className="date-range-prompt">Request for multiple days</span>
        </div>

        <DatePicker
          id="start-date"
          className="date-picker"
          displayMode="portrait"
          value={startDate}
          onChange={(string: string, date: Date) => onStartDateChange(date)}
          formatOptions={{
            year: "numeric",
            month: "long",
            day: "numeric"
          }}
          error={!isValid}
        />
        {isRange && (
          <div className="range-dots">
            <MdMoreVert color="gray" className="range-icon" />
            <MdMoreVert color="gray" className="range-icon" />
            <MdMoreVert color="gray" className="range-icon" />
            <MdMoreVert color="gray" className="range-icon" />
          </div>
        )}

        {isRange && (
          <DatePicker
            id="end-date"
            className="date-picker end-date-picker"
            displayMode="portrait"
            value={endDate}
            onChange={(string: string, date: Date) => onEndDateChange(date)}
            formatOptions={{
              year: "numeric",
              month: "long",
              day: "numeric"
            }}
            error={!isValid}
          />
        )}
      </div>
    );
  }
}

export default DateRange;
