import "date-fns";
import React from "react";
import Radio from "@material-ui/core/Radio";
import RadioGroup from "@material-ui/core/RadioGroup";
import FormControlLabel from "@material-ui/core/FormControlLabel";
import FormControl from "@material-ui/core/FormControl";

import "./styles/SelectCategory.scss";

interface Props {
  category: string | null;
  categories: string[];
  updateCategory: any;
}

export default class SelectCategory extends React.Component<Props> {
  render() {
    const { category, categories, updateCategory } = this.props;
    return (
      <div className="select-category-container">
        <h3 className="section-title">Category</h3>
        <FormControl component="fieldset">
          <RadioGroup
            aria-label="category"
            value={category || "other"}
            onChange={(event: any) => updateCategory(event.target.value)}
            color="primary"
            className="select-categories-group"
          >
            {categories.map(category => (
              <FormControlLabel
                className="category-radio"
                value={category}
                control={<Radio color="primary" style={{ height: "10px" }} />}
                label={category}
                key={category}
                color="primary"
              />
            ))}
            <FormControlLabel
              className="category-radio"
              value="other"
              control={<Radio color="primary" style={{ height: "10px" }} />}
              label="other"
            />
          </RadioGroup>
        </FormControl>
      </div>
    );
  }
}
