import React from "react";
import gql from "graphql-tag";
import { Mutation } from "react-apollo";
import { Button } from "@material-ui/core";
import Modal from "react-modal";

import "./styles/Modal.scss";
import {
  InitialDataQuery_initialData_groups_users,
  InitialDataQuery_initialData_groups
} from "../screens/__generated__/InitialDataQuery";
import {
  RemoveOwnerMutation,
  RemoveOwnerMutationVariables
} from "./__generated__/RemoveOwnerMutation";

const REMOVE_OWNER = gql`
  mutation RemoveOwnerMutation($groupId: String!, $userId: String!) {
    removeOwner(groupId: $groupId, userId: $userId) {
      id
      name
      email
    }
  }
`;
interface Props {
  owner: InitialDataQuery_initialData_groups_users;
  group: InitialDataQuery_initialData_groups;
  isOpen: boolean;
  onRequestClose: any;
  refetchGroups: any;
  customStyles: any;
  makeToast: any;
}

export default class RemoveOwnerModal extends React.Component<Props> {
  onSubmitCompleted = (data: any) => {
    this.props.makeToast(
      "Removed " +
        (this.props.owner.name || this.props.owner.email) +
        " from " +
        this.props.group.name,
      "red"
    );
    this.props.onRequestClose();
    this.props.refetchGroups();
  };

  onSubmitError = (error: any) => {
    console.log(error);
  };

  render() {
    return (
      <Modal
        isOpen={this.props.isOpen}
        onRequestClose={this.props.onRequestClose}
        style={this.props.customStyles}
      >
        <div className="modal-container">
          <h1 className="modal-title">
            Remove {this.props.owner.name || this.props.owner.email} from{" "}
            {this.props.group.name}?
          </h1>
          <div className="modal-content" />

          <Mutation<RemoveOwnerMutation, RemoveOwnerMutationVariables>
            mutation={REMOVE_OWNER}
            variables={{
              groupId: this.props.group.id,
              userId: this.props.owner.id
            }}
            onCompleted={this.onSubmitCompleted}
            onError={this.onSubmitError}
          >
            {(removeOwner: any) => {
              return (
                <div className="modal-buttons-row">
                  <Button
                    color="primary"
                    onClick={this.props.onRequestClose}
                    className="modal-button"
                  >
                    Cancel
                  </Button>
                  <Button
                    color="secondary"
                    variant="contained"
                    onClick={removeOwner}
                    className="modal-button"
                  >
                    Remove
                  </Button>
                </div>
              );
            }}
          </Mutation>
        </div>
      </Modal>
    );
  }
}

Modal.setAppElement("#root");
