export default {
  overlay: {
    position: "fixed",
    backgroundColor: "rgba(255, 255, 255, 0.75)",
    display: "flex",
    flexDirection: "column",
    justifyContent: "center",
    alignItems: "center"
  },
  content: {
    position: "relative",
    border: "1px solid gray",
    borderRadius: "10px",
    padding: "30px",
    top: "unset",
    right: "unset",
    bottom: "unset",
    left: "unset",
    margin: "70px 10px 30px 10px"
  }
};
