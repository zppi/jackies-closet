import React from "react";
import gql from "graphql-tag";
import { Mutation } from "react-apollo";
import { Button, TextField } from "@material-ui/core";
import Modal from "react-modal";

import "./styles/Modal.scss";
import {
  CreateGroupMutation,
  CreateGroupMutationVariables
} from "./__generated__/CreateGroupMutation";

const CREATE_GROUP = gql`
  mutation CreateGroupMutation($name: String!) {
    createGroup(name: $name) {
      name
    }
  }
`;
interface Props {
  isOpen: boolean;
  onRequestClose: any;
  refetchGroups: any;
  customStyles: any;
  makeToast: any;
}
interface State {
  groupName: string;
  errorMessage: string;
}

class CreateGroupModal extends React.Component<Props, State> {
  state: State = {
    groupName: "",
    errorMessage: ""
  };

  componentDidMount() {
    this.setState({ groupName: "", errorMessage: "" });
  }

  onSubmitCompleted = (data: any) => {
    this.props.makeToast("Created the new group " + data.createGroup.name);
    this.props.onRequestClose();
    this.props.refetchGroups();
  };

  onSubmitError = (error: any) => {
    this.setState({ errorMessage: error.graphQLErrors[0].message });
  };

  render() {
    return (
      <Modal
        isOpen={this.props.isOpen}
        onRequestClose={this.props.onRequestClose}
        style={this.props.customStyles}
      >
        <div className="modal-container">
          <h1 className="modal-title">Create a Group</h1>

          <Mutation<CreateGroupMutation, CreateGroupMutationVariables>
            mutation={CREATE_GROUP}
            variables={{
              name: this.state.groupName
            }}
            onCompleted={this.onSubmitCompleted}
            onError={this.onSubmitError}
          >
            {(createGroup: any) => {
              return (
                <div>
                  <div className="modal-content">
                    <TextField
                      className="textfield"
                      label="Group name"
                      variant="outlined"
                      value={this.state.groupName}
                      onChange={event =>
                        this.setState({ groupName: event.target.value })
                      }
                      onKeyPress={(e: any) =>
                        e.key === "Enter" && createGroup()
                      }
                    />
                    <div className="modal-error-message">
                      {this.state.errorMessage}
                    </div>
                  </div>

                  <div className="modal-buttons-row">
                    <Button
                      color="primary"
                      onClick={this.props.onRequestClose}
                      className="modal-button"
                    >
                      Cancel
                    </Button>
                    <Button
                      color="primary"
                      variant="contained"
                      onClick={createGroup}
                      className="modal-button"
                      disabled={this.state.groupName.length < 5}
                    >
                      Create Group
                    </Button>
                  </div>
                </div>
              );
            }}
          </Mutation>
        </div>
      </Modal>
    );
  }
}

export default CreateGroupModal;

Modal.setAppElement("#root");
