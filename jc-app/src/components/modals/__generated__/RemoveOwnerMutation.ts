/* tslint:disable */
/* eslint-disable */
// This file was automatically generated and should not be edited.

// ====================================================
// GraphQL mutation operation: RemoveOwnerMutation
// ====================================================

export interface RemoveOwnerMutation_removeOwner {
  __typename: "User";
  id: string;
  name: string | null;
}

export interface RemoveOwnerMutation {
  removeOwner: RemoveOwnerMutation_removeOwner;
}

export interface RemoveOwnerMutationVariables {
  groupId: string;
  userId: string;
}
