/* tslint:disable */
/* eslint-disable */
// This file was automatically generated and should not be edited.

// ====================================================
// GraphQL mutation operation: JoinGroupMutation
// ====================================================

export interface JoinGroupMutation_addToGroup_group {
  __typename: "Group";
  id: string;
  name: string;
}

export interface JoinGroupMutation_addToGroup_users {
  __typename: "User";
  id: string;
  email: string;
}

export interface JoinGroupMutation_addToGroup {
  __typename: "GroupUsers";
  group: JoinGroupMutation_addToGroup_group;
  users: JoinGroupMutation_addToGroup_users[];
}

export interface JoinGroupMutation {
  addToGroup: JoinGroupMutation_addToGroup;
}

export interface JoinGroupMutationVariables {
  groupId: string;
  emails: string[];
}
