/* tslint:disable */
/* eslint-disable */
// This file was automatically generated and should not be edited.

// ====================================================
// GraphQL query operation: GroupsListQuery
// ====================================================

export interface GroupsListQuery_groupsToJoin {
  __typename: "Group";
  id: string;
  name: string;
}

export interface GroupsListQuery {
  groupsToJoin: GroupsListQuery_groupsToJoin[];
}
