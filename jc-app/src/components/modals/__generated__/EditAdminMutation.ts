/* tslint:disable */
/* eslint-disable */
// This file was automatically generated and should not be edited.

// ====================================================
// GraphQL mutation operation: EditAdminMutation
// ====================================================

export interface EditAdminMutation_editAdmin {
  __typename: "Group";
  id: string;
}

export interface EditAdminMutation {
  editAdmin: EditAdminMutation_editAdmin;
}

export interface EditAdminMutationVariables {
  userId: string;
  groupId: string;
  shouldBeAdmin?: boolean | null;
}
