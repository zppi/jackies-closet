/* tslint:disable */
/* eslint-disable */
// This file was automatically generated and should not be edited.

// ====================================================
// GraphQL mutation operation: CreateGroupMutation
// ====================================================

export interface CreateGroupMutation_createGroup {
  __typename: "Group";
  name: string;
}

export interface CreateGroupMutation {
  createGroup: CreateGroupMutation_createGroup;
}

export interface CreateGroupMutationVariables {
  name: string;
}
