/* tslint:disable */
/* eslint-disable */
// This file was automatically generated and should not be edited.

// ====================================================
// GraphQL mutation operation: DeleteItemMutation
// ====================================================

export interface DeleteItemMutation_deleteItem_group {
  __typename: "Group";
  id: string;
  name: string;
}

export interface DeleteItemMutation_deleteItem {
  __typename: "Item";
  id: string;
  name: string;
  category: string | null;
  group: DeleteItemMutation_deleteItem_group;
}

export interface DeleteItemMutation {
  deleteItem: DeleteItemMutation_deleteItem;
}

export interface DeleteItemMutationVariables {
  id: string;
}
