import React from "react";
import gql from "graphql-tag";
import { Mutation } from "react-apollo";
import { Button } from "@material-ui/core";
import Modal from "react-modal";

import "./styles/Modal.scss";
import {
  InitialDataQuery_initialData_groups,
  InitialDataQuery_initialData_groups_users
} from "../screens/__generated__/InitialDataQuery";
import {
  EditAdminMutation,
  EditAdminMutationVariables
} from "./__generated__/EditAdminMutation";

interface Props {
  user: InitialDataQuery_initialData_groups_users;
  group: InitialDataQuery_initialData_groups;
  isYou: boolean;
  isAdmin: boolean;
  isOpen: boolean;
  onRequestClose: any;
  refetchGroups: any;
  makeToast: any;
  customStyles: any;
}

const EDIT_ADMIN = gql`
  mutation EditAdminMutation(
    $userId: String!
    $groupId: String!
    $shouldBeAdmin: Boolean
  ) {
    editAdmin(
      userId: $userId
      groupId: $groupId
      shouldBeAdmin: $shouldBeAdmin
    ) {
      id
    }
  }
`;

export default class OwnerModal extends React.Component<Props> {
  onSubmitCompleted = (data: any) => {
    if (this.props.isAdmin) {
      this.props.makeToast(
        "Removed admin privileges from " +
          this.props.user.name +
          " for " +
          this.props.group.name,
        "red"
      );
    } else {
      this.props.makeToast(
        "Granted admin privileges to " +
          this.props.user.name +
          " in " +
          this.props.group.name
      );
    }
    this.props.onRequestClose();
    this.props.refetchGroups();
  };

  onSubmitError = (error: any) => {
    console.log(error);
  };

  render() {
    return (
      <Modal
        isOpen={this.props.isOpen}
        onRequestClose={this.props.onRequestClose}
        style={this.props.customStyles}
      >
        <div className="modal-container">
          <h1 className="modal-title">{this.props.user.name}</h1>
          <div>
            <div className="modal-content">
              <div className="labels-container">
                <div className="label-section">
                  <span className="label">Name: </span>
                  <span className="value">{this.props.user.name}</span>
                </div>
                <div className="label-section">
                  <span className="label">Email: </span>
                  <span className="value">{this.props.user.email}</span>
                </div>
                <div className="label-section">
                  <span className="label">Group: </span>
                  <span className="value">{this.props.group.name}</span>
                </div>
              </div>
            </div>

            <div className="modal-buttons-row">
              <Button
                color="primary"
                onClick={this.props.onRequestClose}
                className="modal-button"
              >
                Close
              </Button>
              {!this.props.isYou && (
                <Mutation<EditAdminMutation, EditAdminMutationVariables>
                  mutation={EDIT_ADMIN}
                  variables={{
                    userId: this.props.user.id,
                    groupId: this.props.group.id,
                    shouldBeAdmin: !this.props.isAdmin
                  }}
                  onCompleted={this.onSubmitCompleted}
                  onError={this.onSubmitError}
                >
                  {(editAdmin: any) => {
                    return (
                      <Button
                        color="primary"
                        variant="contained"
                        onClick={() => editAdmin()}
                        className="modal-button"
                      >
                        {this.props.isAdmin ? "Remove Admin" : "Make Admin"}
                      </Button>
                    );
                  }}
                </Mutation>
              )}
            </div>
          </div>
        </div>
      </Modal>
    );
  }
}

Modal.setAppElement("#root");
