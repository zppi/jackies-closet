import React from "react";
import gql from "graphql-tag";
import { Mutation, graphql, ChildProps } from "react-apollo";
import { Button, TextField, Paper, MenuItem } from "@material-ui/core";
import Modal from "react-modal";

import "./styles/Modal.scss";
import "./styles/Dropdown.css";

import {
  JoinGroupMutation,
  JoinGroupMutationVariables
} from "./__generated__/JoinGroupMutation";
import {
  GroupsListQuery,
  GroupsListQuery_groupsToJoin
} from "./__generated__/GroupsListQuery";

const JOIN_GROUP = gql`
  mutation JoinGroupMutation($groupId: String!, $emails: [String!]!) {
    addToGroup(id: $groupId, emails: $emails) {
      group {
        id
        name
      }
      users {
        id
        email
      }
    }
  }
`;
interface Props {
  email: string;
  isOpen: boolean;
  onRequestClose: any;
  refetchGroups: any;
  customStyles: any;
}
interface State {
  searchQuery: string;
  groupId: string;
  menuOpen: boolean;
  isHoveringOption: boolean;
}

// @ts-ignore
@graphql(gql`
  query GroupsListQuery {
    groupsToJoin {
      id
      name
    }
  }
`)
// @ts-ignore
class JoinGroupModal extends React.Component<
  ChildProps<Props, GroupsListQuery>,
  State
> {
  state: State = {
    searchQuery: "",
    groupId: "",
    menuOpen: false,
    isHoveringOption: false
  };

  componentDidMount() {
    this.setState({
      searchQuery: "",
      groupId: "",
      menuOpen: false,
      isHoveringOption: false
    });
  }

  onSubmitCompleted = (data: any) => {
    // this.setState({ submitting: false });
    this.props.onRequestClose();
    this.props.refetchGroups();
  };

  onSubmitError = (error: any) => {
    console.log(error);
  };

  filterGroups(
    groups: GroupsListQuery_groupsToJoin[]
  ): GroupsListQuery_groupsToJoin[] {
    let matchingGroups: GroupsListQuery_groupsToJoin[] = [];
    groups.forEach((group: GroupsListQuery_groupsToJoin) => {
      if (
        group.name.toLowerCase().includes(this.state.searchQuery.toLowerCase())
      ) {
        matchingGroups.push(group);
      }
    });
    return matchingGroups;
  }

  render() {
    if (!this.props.data || !this.props.data.groupsToJoin) {
      return null;
    }
    const groups: GroupsListQuery_groupsToJoin[] = this.props.data.groupsToJoin;
    return (
      <Modal
        isOpen={this.props.isOpen}
        onRequestClose={this.props.onRequestClose}
        style={this.props.customStyles}
      >
        <div className="modal-container">
          <h1 className="modal-title">Join a Group</h1>
          <div>
            <div className="modal-content">
              {this.renderInput()}
              <div className="contained-dropdown">
                {this.state.menuOpen && (
                  <Paper
                    style={{
                      position: "relative",
                      zIndex: 1,
                      left: 0,
                      right: 0
                    }}
                    square
                  >
                    {this.filterGroups(groups).map(
                      (group: GroupsListQuery_groupsToJoin) => (
                        <MenuItem
                          key={group.id}
                          component="div"
                          onClick={() => this.onGroupClicked(group)}
                          onMouseMove={this.onGroupHovered}
                          onMouseOut={this.onGroupNotHovered}
                        >
                          {group.name}
                        </MenuItem>
                      )
                    )}
                  </Paper>
                )}
              </div>
              <div className="error-message" />
            </div>
            <div className="modal-buttons-row">
              <Button
                color="primary"
                onClick={this.props.onRequestClose}
                className="modal-button"
              >
                Cancel
              </Button>
              <Mutation<JoinGroupMutation, JoinGroupMutationVariables>
                mutation={JOIN_GROUP}
                variables={{
                  groupId: this.state.groupId,
                  emails: [this.props.email]
                }}
                onCompleted={this.onSubmitCompleted}
                onError={this.onSubmitError}
              >
                {(joinGroup: any) => {
                  return (
                    <Button
                      color="primary"
                      variant="contained"
                      onClick={joinGroup}
                      className="modal-button"
                      disabled={this.state.groupId === ""}
                    >
                      Join Group
                    </Button>
                  );
                }}
              </Mutation>
            </div>
          </div>
        </div>
      </Modal>
    );
  }
  renderInput() {
    const onBlur = () =>
      !this.state.isHoveringOption && this.setState({ menuOpen: false });
    const onChange = (event: any) =>
      this.setState({ searchQuery: event.target.value });
    const onFocus = () => this.setState({ menuOpen: true });
    return (
      <TextField
        className="textField"
        value={this.state.searchQuery}
        label="Find Group"
        InputProps={{
          onBlur,
          onChange,
          onFocus
        }}
        style={{ width: "100%" }}
      />
    );
  }

  onGroupClicked = (group: GroupsListQuery_groupsToJoin): void => {
    this.setState({
      groupId: group.id,
      searchQuery: group.name,
      isHoveringOption: false,
      menuOpen: false
    });
  };

  onGroupHovered = (): void => {
    !this.state.isHoveringOption && this.setState({ isHoveringOption: true });
  };

  onGroupNotHovered = (): void => {
    this.setState({ isHoveringOption: false });
  };
}

export default JoinGroupModal;

Modal.setAppElement("#root");
