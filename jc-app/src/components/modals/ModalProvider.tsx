import React from "react";

import {
  InitialDataQuery_initialData_groups,
  InitialDataQuery_initialData_groups_items,
  InitialDataQuery_initialData_groups_users
} from "../screens/__generated__/InitialDataQuery";

import {
  DeleteItemModal,
  CreateGroupModal,
  RemoveOwnerModal,
  OwnerModal,
  ModalStyles
} from ".";

interface Props {
  groups: InitialDataQuery_initialData_groups[];
  searchQuery: string;
  refetchGroups: any;
  makeToast: any;
  history: any;
}

interface State {
  selectedGroup: InitialDataQuery_initialData_groups | null;
  selectedItem: InitialDataQuery_initialData_groups_items | null;
  selectedOwner: InitialDataQuery_initialData_groups_users | null;
  isYours: boolean;
  isYou: boolean;
  userIsAdmin: boolean;
  createGroupModalIsOpen: boolean;
  deleteItemModalIsOpen: boolean;
  removeOwnerModalIsOpen: boolean;
  ownerModalIsOpen: boolean;
}

export default class ModalProvider extends React.Component<Props, State> {
  state: State = {
    selectedGroup: null,
    selectedItem: null,
    selectedOwner: null,
    isYours: false,
    isYou: false,
    userIsAdmin: false,
    createGroupModalIsOpen: false,
    deleteItemModalIsOpen: false,
    removeOwnerModalIsOpen: false,
    ownerModalIsOpen: false
  };
  componentDidMount() {
    this.setState({
      selectedGroup: null,
      selectedItem: null,
      selectedOwner: null,
      isYours: false,
      isYou: false,
      userIsAdmin: false,
      createGroupModalIsOpen: false,
      deleteItemModalIsOpen: false,
      removeOwnerModalIsOpen: false,
      ownerModalIsOpen: false
    });
  }
  openCreateGroupModal = (): void => {
    this.setState({ createGroupModalIsOpen: true });
  };
  closeCreateGroupModal = (): void => {
    this.setState({ selectedGroup: null, createGroupModalIsOpen: false });
  };
  openDeleteItemModal = (
    item: InitialDataQuery_initialData_groups_items
  ): void => {
    this.setState({ selectedItem: item, deleteItemModalIsOpen: true });
  };
  closeDeleteItemModal = (): void => {
    this.setState({ selectedItem: null, deleteItemModalIsOpen: false });
  };
  openRemoveOwnerModal = (
    owner: InitialDataQuery_initialData_groups_users,
    group: InitialDataQuery_initialData_groups
  ): void => {
    this.setState({
      selectedOwner: owner,
      selectedGroup: group,
      removeOwnerModalIsOpen: true
    });
  };
  closeRemoveOwnerModal = (): void => {
    this.setState({
      selectedOwner: null,
      selectedGroup: null,
      removeOwnerModalIsOpen: false
    });
  };
  openOwnerModal = (
    owner: InitialDataQuery_initialData_groups_users,
    group: InitialDataQuery_initialData_groups,
    isYou: boolean,
    isAdmin: boolean
  ): void => {
    this.setState({
      ownerModalIsOpen: true,
      selectedOwner: owner,
      selectedGroup: group,
      isYou,
      userIsAdmin: isAdmin
    });
  };
  closeOwnerModal = (): void => {
    this.setState({
      ownerModalIsOpen: false,
      selectedOwner: null,
      selectedGroup: null,
      isYou: false,
      userIsAdmin: false
    });
  };

  render() {
    return (
      <>
        <CreateGroupModal
          isOpen={this.state.createGroupModalIsOpen}
          onRequestClose={this.closeCreateGroupModal}
          refetchGroups={() => this.props.refetchGroups()}
          makeToast={this.props.makeToast}
          customStyles={ModalStyles}
        />
        {this.state.selectedItem && (
          <DeleteItemModal
            item={this.state.selectedItem}
            isOpen={this.state.deleteItemModalIsOpen}
            onRequestClose={this.closeDeleteItemModal}
            refetchGroups={() => this.props.refetchGroups()}
            makeToast={this.props.makeToast}
            customStyles={ModalStyles}
          />
        )}
        {this.state.selectedOwner && this.state.selectedGroup && (
          <RemoveOwnerModal
            owner={this.state.selectedOwner}
            group={this.state.selectedGroup}
            isOpen={this.state.removeOwnerModalIsOpen}
            onRequestClose={this.closeRemoveOwnerModal}
            refetchGroups={() => this.props.refetchGroups()}
            makeToast={this.props.makeToast}
            customStyles={ModalStyles}
          />
        )}
        {this.state.selectedOwner && this.state.selectedGroup && (
          <OwnerModal
            user={this.state.selectedOwner}
            group={this.state.selectedGroup}
            isYou={this.state.isYou}
            isAdmin={this.state.userIsAdmin}
            isOpen={this.state.ownerModalIsOpen}
            onRequestClose={this.closeOwnerModal}
            refetchGroups={() => this.props.refetchGroups()}
            makeToast={this.props.makeToast}
            customStyles={ModalStyles}
          />
        )}
      </>
    );
  }
}
