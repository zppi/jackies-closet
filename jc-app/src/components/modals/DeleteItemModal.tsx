import React from "react";
import gql from "graphql-tag";
import { Mutation } from "react-apollo";
import { Button } from "@material-ui/core";
import Modal from "react-modal";

import "./styles/Modal.scss";
import { InitialDataQuery_initialData_groups_items } from "../screens/__generated__/InitialDataQuery";
import {
  DeleteItemMutation,
  DeleteItemMutationVariables
} from "./__generated__/DeleteItemMutation";

const DELETE_ITEM = gql`
  mutation DeleteItemMutation($id: String!) {
    deleteItem(id: $id) {
      id
      name
      category
      group {
        id
        name
      }
    }
  }
`;
interface Props {
  item: InitialDataQuery_initialData_groups_items;
  isOpen: boolean;
  makeToast: any;
  onRequestClose: any;
  refetchGroups: any;
  customStyles: any;
}

class DeleteItemModal extends React.Component<Props> {
  onSubmitCompleted = (data: any) => {
    this.props.makeToast(
      "Deleted " +
        data.deleteItem.name +
        " from " +
        (data.deleteItem.category ? data.deleteItem.category + " in " : "") +
        this.props.item.group.name,
      "red"
    );
    this.props.onRequestClose();
    this.props.refetchGroups();
  };

  onSubmitError = (error: any) => {
    console.log(error);
  };

  render() {
    return (
      <Modal
        isOpen={this.props.isOpen}
        onRequestClose={this.props.onRequestClose}
        style={this.props.customStyles}
      >
        <div className="modal-container">
          <h1 className="modal-title">Delete {this.props.item.name}?</h1>
          <div className="modal-content" />

          <Mutation<DeleteItemMutation, DeleteItemMutationVariables>
            mutation={DELETE_ITEM}
            variables={{
              id: this.props.item.id
            }}
            onCompleted={this.onSubmitCompleted}
            onError={this.onSubmitError}
          >
            {(deleteItem: any) => {
              return (
                <div className="modal-buttons-row">
                  <Button
                    color="primary"
                    onClick={this.props.onRequestClose}
                    className="modal-button"
                  >
                    Cancel
                  </Button>
                  <Button
                    color="secondary"
                    variant="contained"
                    onClick={deleteItem}
                    className="modal-button"
                  >
                    Delete
                  </Button>
                </div>
              );
            }}
          </Mutation>
        </div>
      </Modal>
    );
  }
}

export default DeleteItemModal;

Modal.setAppElement("#root");
