import React from "react";

import "./styles/Empty.css";

interface Props {
  message: any;
}

export default class Empty extends React.Component<Props> {
  render() {
    return (
      <div className="empty-container">
        <div className="empty-text">{this.props.message}</div>
      </div>
    );
  }
}
