import React from "react";
import { TextField } from "@material-ui/core";
import Checkbox from "@material-ui/core/Checkbox";

import { SelectCategory } from "./";
import "./modals/styles/Modal.scss";

interface Props {
  item: any;
  updateItem: any;
}

export default class EditItemSpecifics extends React.Component<Props> {
  handleNameChange = (event: any) => {
    const item = this.props.item;
    item.name = event.target.value;
    this.props.updateItem(item);
  };
  handleSharedChange = (event: any) => {
    const item = this.props.item;
    item.isShared = event.target.checked;
    this.props.updateItem(item);
  };
  handleCategoryChange = (category: string) => {
    const item = this.props.item;
    item.category = category;
    this.props.updateItem(item);
  };

  render() {
    const { item } = this.props;
    return (
      <div>
        <TextField
          className="textfield"
          label="Item name"
          variant="outlined"
          value={item.name}
          onChange={event => this.handleNameChange(event)}
        />
        <div className="checkbox-section">
          <Checkbox
            checked={item.isShared}
            onChange={event => this.handleSharedChange(event)}
            color="primary"
          />
          <span className="checkbox-prompt">Share with {item.group.name}</span>
        </div>
        <div className="section-container">
          <SelectCategory
            category={item.category}
            categories={item.group.categories}
            updateCategory={(category: string) =>
              this.handleCategoryChange(category)
            }
          />
        </div>
      </div>
    );
  }
}
