/* tslint:disable */
/* eslint-disable */
// This file was automatically generated and should not be edited.

// ====================================================
// GraphQL mutation operation: FillProfileMutation
// ====================================================

export interface FillProfileMutation_editProfile {
  __typename: "EditProfilePayload";
  name: string;
}

export interface FillProfileMutation {
  editProfile: FillProfileMutation_editProfile;
}

export interface FillProfileMutationVariables {
  name: string;
}
