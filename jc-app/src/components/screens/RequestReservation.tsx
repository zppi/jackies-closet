import React from "react";
import gql from "graphql-tag";
import { Query, Mutation } from "react-apollo";
import { Button, TextField } from "@material-ui/core";
import Modal from "react-modal";
import Checkbox from "@material-ui/core/Checkbox";

import { DateRange, NavBar } from "..";
import "../modals/styles/Modal.scss";
import "../modals/styles/RequestModal.scss";
import {
  RequestReservationMutation,
  RequestReservationMutationVariables
} from "./__generated__/RequestReservationMutation";

const REQUEST_RESERVATION = gql`
  mutation RequestReservationMutation(
    $itemId: String!
    $startDate: Int!
    $endDate: Int
    $note: String!
  ) {
    requestReservation(
      itemId: $itemId
      startDate: $startDate
      endDate: $endDate
      note: $note
    ) {
      successful
    }
  }
`;

const GET_ITEM = gql`
  query RequestReservationQuery($id: String!) {
    item(id: $id) {
      id
      name
      isShared
      category
      group {
        id
        name
      }
    }
  }
`;

interface Props {
  history: any;
  match: any;
}

interface State {
  startDate: Date;
  endDate: Date;
  isRange: boolean;
  rangeIsValid: boolean;
  isCareAgreed: boolean;
  isTimelinessAgreed: boolean;
  noteText: string;
}

class RequestReservationModal extends React.Component<Props, State> {
  state: State = {
    startDate: new Date(),
    endDate: new Date(new Date().getTime() + 24 * 60 * 60 * 1000),
    isRange: false,
    rangeIsValid: true,
    isCareAgreed: false,
    isTimelinessAgreed: false,
    noteText: ""
  };

  componentDidMount() {
    this.setState({
      startDate: new Date(),
      endDate: new Date(new Date().getTime() + 24 * 60 * 60 * 1000),
      isRange: false,
      rangeIsValid: true,
      isCareAgreed: false,
      isTimelinessAgreed: false,
      noteText: ""
    });
  }

  onSubmitCompleted = (data: any) => {
    this.props.history.goBack();
    this.props.history.goBack();
  };

  onSubmitError = (error: any) => {
    console.log(error);
  };

  handleStartDateChange = (startDate: Date) => {
    this.setState({ startDate }, () =>
      this.setState({ rangeIsValid: this.isRangeValid() })
    );
  };

  handleEndDateChange = (endDate: Date) => {
    this.setState({ endDate }, () =>
      this.setState({ rangeIsValid: this.isRangeValid() })
    );
  };

  isRangeValid() {
    const yesterday = new Date(new Date().getTime() - 24 * 60 * 60 * 1000);
    const today = new Date();
    return (
      this.state.startDate.getTime() > yesterday.getTime() &&
      (!this.state.isRange ||
        (this.state.endDate.getTime() > this.state.startDate.getTime() &&
          this.state.endDate.getTime() > today.getTime()))
    );
  }

  handleIsRangeChange = (event: any) => {
    this.setState({ isRange: event.target.checked }, () =>
      this.setState({ rangeIsValid: this.isRangeValid() })
    );
  };

  handleCareChange = (event: any) => {
    this.setState({ isCareAgreed: event.target.checked });
  };

  handleTimelinessChange = (event: any) => {
    this.setState({ isTimelinessAgreed: event.target.checked });
  };

  updateNoteText = (noteText: string) => {
    this.setState({ noteText });
  };

  convertDate(date: Date): number {
    const newDate = new Date(
      date.getTime() + date.getTimezoneOffset() * 60 * 1000
    );
    return newDate.getTime() / 1000;
  }

  render() {
    const { itemId } = this.props.match.params;
    const {
      startDate,
      endDate,
      isRange,
      rangeIsValid,
      isCareAgreed,
      isTimelinessAgreed,
      noteText
    } = this.state;
    return (
      <div className="container">
        <NavBar history={this.props.history} />
        <Query query={GET_ITEM} variables={{ id: itemId }}>
          {({ loading, error, data }: any) => {
            if (loading) {
              return null;
            }
            const item: any = data.item;
            if (!item) {
              this.props.history.goBack();
              this.props.history.goBack();
              return null;
            }
            return (
              <div className="content modal-container">
                <h1 className="modal-title">
                  Request <b>{item.name}</b>
                </h1>
                <div className="modal-content">
                  <div className="date-section">
                    <div className="section-container">
                      <h3 className="section-title">Select dates</h3>
                      <DateRange
                        startDate={startDate}
                        endDate={endDate}
                        onStartDateChange={this.handleStartDateChange}
                        onEndDateChange={this.handleEndDateChange}
                        isRange={isRange}
                        onIsRangeChange={this.handleIsRangeChange}
                        isValid={rangeIsValid}
                      />
                    </div>
                  </div>

                  <div className="rule-section section-container">
                    <h3 className="section-title">Review rules</h3>
                    <div className="checkbox-section">
                      <Checkbox
                        checked={isCareAgreed}
                        onChange={(event: any) => this.handleCareChange(event)}
                        color="primary"
                      />
                      <span className="checkbox-prompt">
                        I will treat the items I borrow with respect and care.
                      </span>
                    </div>
                    <div className="checkbox-section">
                      <Checkbox
                        checked={isTimelinessAgreed}
                        onChange={(event: any) =>
                          this.handleTimelinessChange(event)
                        }
                        color="primary"
                      />
                      <span className="checkbox-prompt">
                        I will arrive to pick up and return the items I borrow
                        at the agreed time.
                      </span>
                    </div>
                  </div>

                  <div className="note-section section-container">
                    <h3 className="section-title">Leave a note</h3>
                    <TextField
                      id="note"
                      multiline={true}
                      placeholder="Provide some details..."
                      variant="outlined"
                      value={noteText}
                      onChange={event =>
                        this.updateNoteText(event.target.value)
                      }
                      className="note"
                    />
                  </div>
                </div>

                <Mutation<
                  RequestReservationMutation,
                  RequestReservationMutationVariables
                >
                  mutation={REQUEST_RESERVATION}
                  variables={{
                    itemId: item.id,
                    startDate: this.convertDate(startDate),
                    endDate: isRange ? this.convertDate(endDate) : null,
                    note: noteText
                  }}
                  onCompleted={this.onSubmitCompleted}
                  onError={this.onSubmitError}
                >
                  {(requestReservation: any) => {
                    return (
                      <div className="modal-buttons-row">
                        <Button
                          color="primary"
                          onClick={() => {
                            this.props.history.goBack();
                            this.props.history.goBack();
                          }}
                          className="modal-button"
                        >
                          Cancel
                        </Button>
                        <Button
                          color="primary"
                          variant="contained"
                          onClick={requestReservation}
                          className="modal-button"
                          disabled={
                            !rangeIsValid ||
                            !isCareAgreed ||
                            !isTimelinessAgreed
                          }
                        >
                          Request
                        </Button>
                      </div>
                    );
                  }}
                </Mutation>
              </div>
            );
          }}
        </Query>
      </div>
    );
  }
}

export default RequestReservationModal;

Modal.setAppElement("#root");
