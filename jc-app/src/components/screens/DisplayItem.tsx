import React from "react";
import gql from "graphql-tag";
import { Query } from "react-apollo";
import { Button } from "@material-ui/core";

import "../modals/styles/Modal.scss";
import { NavBar } from "../";

interface Props {
  history: any;
  match: any;
}

const GET_ITEM = gql`
  query DisplayItemQuery($id: String!) {
    item(id: $id) {
      id
      name
      isShared
      category
      group {
        id
        name
        categories
      }
      user {
        id
        name
      }
    }
    initialData {
      id
    }
  }
`;

export default class DisplayItem extends React.Component<Props> {
  isYours(item: any, initialData: any): boolean {
    return item.user.id === initialData.id;
  }

  render() {
    const { itemId } = this.props.match.params;

    return (
      <div className="container">
        <NavBar history={this.props.history} />
        <Query query={GET_ITEM} variables={{ id: itemId }}>
          {({ loading, error, data }: any) => {
            if (loading) {
              return null;
            }
            const item: any = data.item;
            const initialData: any = data.initialData;
            if (!item || !initialData) {
              this.props.history.goBack();
              return null;
            }
            return (
              <div className="content modal-container">
                <h1 className="modal-title">{item.name}</h1>
                <div>
                  <div className="modal-content">
                    <div className="labels-container">
                      <div className="label-section">
                        <span className="label">Owner: </span>
                        <span className="value">{item.user.name}</span>
                      </div>
                      <div className="label-section">
                        <span className="label">Category: </span>
                        <span className="value">
                          {item.category || "other"}
                        </span>
                      </div>
                    </div>
                  </div>

                  <div className="modal-buttons-row">
                    <Button
                      color="primary"
                      onClick={() => this.props.history.goBack()}
                      className="modal-button"
                    >
                      Cancel
                    </Button>
                    {this.isYours(item, initialData) ? (
                      <Button
                        color="primary"
                        variant="contained"
                        onClick={async () => {
                          this.props.history.push("/edit-item/" + item.id);
                        }}
                        className="modal-button"
                      >
                        Edit
                      </Button>
                    ) : (
                      <>
                        {item.isShared && (
                          <Button
                            color="primary"
                            variant="contained"
                            onClick={async () => {
                              this.props.history.push(
                                "/request-item/" + item.id
                              );
                            }}
                            className="modal-button"
                          >
                            Request
                          </Button>
                        )}
                      </>
                    )}
                  </div>
                </div>
              </div>
            );
          }}
        </Query>
      </div>
    );
  }
}
