import React from "react";
import gql from "graphql-tag";
import { Query, Mutation } from "react-apollo";
import { Button } from "@material-ui/core";

import "../modals/styles/Modal.scss";
import { InitialDataQuery_initialData_groups_items } from "../screens/__generated__/InitialDataQuery";
import {
  EditItemMutation,
  EditItemMutationVariables
} from "./__generated__/EditItemMutation";
import { EditItemSpecifics, NavBar } from "../";

const EDIT_ITEM = gql`
  mutation EditItemMutation(
    $id: String!
    $name: String!
    $category: String
    $isShared: Boolean
  ) {
    editItem(id: $id, name: $name, category: $category, isShared: $isShared) {
      id
      name
      category
      isShared
    }
  }
`;
interface Props {
  history: any;
  match: any;
}
interface State {
  item: InitialDataQuery_initialData_groups_items | null;
  errorMessage: string;
}

const GET_ITEM = gql`
  query EditItemQuery($id: String!) {
    item(id: $id) {
      id
      name
      isShared
      category
      group {
        id
        name
        categories
      }
    }
  }
`;

export default class EditItem extends React.Component<Props, State> {
  state: State = {
    item: null,
    errorMessage: ""
  };

  componentDidMount() {
    this.setState({
      item: null,
      errorMessage: ""
    });
  }

  onSubmitCompleted = (data: any) => {
    this.props.history.goBack();
    this.props.history.goBack();
  };

  onSubmitError = (error: any) => {
    this.setState({ errorMessage: error.graphQLErrors[0].message });
  };

  render() {
    const { itemId } = this.props.match.params;

    return (
      <div className="container">
        <NavBar history={this.props.history} />
        <Query query={GET_ITEM} variables={{ id: itemId }}>
          {({ loading, error, data }: any) => {
            if (loading) {
              return null;
            }
            const item: any = data.item;
            if (!item) {
              this.props.history.goBack();
              return null;
            }
            return (
              <div className="content modal-container">
                <h1 className="modal-title">Edit {item.name}</h1>

                <Mutation<EditItemMutation, EditItemMutationVariables>
                  mutation={EDIT_ITEM}
                  variables={
                    this.state.item
                      ? {
                          id: item.id,
                          name: this.state.item.name,
                          category: this.state.item.category,
                          isShared: this.state.item.isShared
                        }
                      : {
                          id: item.id,
                          name: item.name,
                          category: item.category,
                          isShared: item.isShared
                        }
                  }
                  onCompleted={this.onSubmitCompleted}
                  onError={this.onSubmitError}
                >
                  {(editItem: any) => {
                    return (
                      <div>
                        <div className="modal-content">
                          <EditItemSpecifics
                            item={this.state.item || Object.assign({}, item)}
                            updateItem={(
                              item: InitialDataQuery_initialData_groups_items
                            ) => this.setState({ item })}
                          />
                          {this.state.errorMessage && (
                            <div className="modal-error-message">
                              {this.state.errorMessage}
                            </div>
                          )}
                        </div>

                        <div className="modal-buttons-row">
                          <Button
                            color="primary"
                            onClick={() => {
                              this.props.history.goBack();
                              this.props.history.goBack();
                            }}
                            className="modal-button"
                          >
                            Cancel
                          </Button>
                          <Button
                            color="primary"
                            variant="contained"
                            onClick={editItem}
                            className="modal-button"
                          >
                            Save
                          </Button>
                        </div>
                      </div>
                    );
                  }}
                </Mutation>
              </div>
            );
          }}
        </Query>
      </div>
    );
  }
}
