import React from "react";
import gql from "graphql-tag";
import { graphql, ChildProps } from "react-apollo";
import { Button } from "@material-ui/core";
import { Tab, Tabs, TabList, TabPanel } from "react-tabs";
import "react-tabs/style/react-tabs.css";

import { getSessionToken, signOut } from "../../services/AuthService";
import {
  InitialDataQuery,
  InitialDataQuery_initialData,
  InitialDataQuery_initialData_groups,
  InitialDataQuery_initialData_groups_users
} from "./__generated__/InitialDataQuery";
import { NavBar, Empty, Group, FillProfile } from "../";
import { ModalProvider } from "../modals";

import "./styles/Home.scss";

interface Props {
  history: any;
  makeToast: any;
}

interface State {
  searchQuery: string;
  selectedGroup: InitialDataQuery_initialData_groups | null;
  menuAnchor: any;
  modals: any;
}

// @ts-ignore
@graphql(gql`
  query InitialDataQuery {
    initialData {
      id
      name
      email
      groups {
        name
        id
        items {
          name
          id
          user {
            name
            id
          }
          category
          group {
            id
            categories
            name
          }
          isShared
        }
        users {
          name
          email
          id
          items {
            name
            id
            group {
              id
            }
          }
        }
        adminIds
        categories
      }
    }
  }
`)
// @ts-ignore
export default class Home extends React.Component<
  ChildProps<Props, InitialDataQuery>,
  State
> {
  state: State = {
    searchQuery: "",
    selectedGroup: null,
    menuAnchor: null,
    modals: null
  };
  componentWillMount() {
    this.setState({
      selectedGroup: null,
      menuAnchor: null,
      modals: null
    });
    if (this.props.data && this.props.data.networkStatus !== 4) {
      this.props.data.refetch();
    }
  }
  componentDidMount() {
    getSessionToken().then((token: any) => {
      if (!token) {
        this.props.history.push("/login");
      }
    });
  }

  signMeOut = () => {
    signOut();
    this.props.history.push("/login");
  };

  openMenu = (event: any, group: InitialDataQuery_initialData_groups) => {
    this.setState({ menuAnchor: event.currentTarget, selectedGroup: group });
  };

  closeMenu = () => {
    this.setState({ menuAnchor: null, selectedGroup: null });
  };

  isAdmin(
    user:
      | InitialDataQuery_initialData
      | InitialDataQuery_initialData_groups_users,
    group: InitialDataQuery_initialData_groups
  ): boolean {
    return group.adminIds.includes(user.id);
  }

  renderGroupItems(groups: InitialDataQuery_initialData_groups[]) {
    if (!this.props.data || !this.state.modals) {
      return null;
    }
    return groups.map((group: InitialDataQuery_initialData_groups) => {
      return (
        <Group
          searchQuery={this.state.searchQuery}
          group={group}
          // @ts-ignore
          user={this.props.data.initialData}
          openMenu={this.openMenu}
          closeMenu={this.closeMenu}
          modals={this.state.modals}
          showItems={true}
          menuAnchor={
            this.state.selectedGroup && this.state.selectedGroup.id === group.id
              ? this.state.menuAnchor
              : null
          }
          isAdmin={
            this.props.data && this.props.data.initialData
              ? this.isAdmin(this.props.data.initialData, group)
              : false
          }
          refetchGroups={() => this.props.data && this.props.data.refetch()}
          history={this.props.history}
          key={group.id}
        />
      );
    });
  }

  renderGroupOwners(groups: InitialDataQuery_initialData_groups[]) {
    if (!this.props.data || !this.state.modals) {
      return null;
    }
    let isAnAdmin = false;
    let groupsMap = groups.map((group: InitialDataQuery_initialData_groups) => {
      const isAdmin =
        this.props.data && this.props.data.initialData
          ? this.isAdmin(this.props.data.initialData, group)
          : false;
      if (isAdmin) {
        isAnAdmin = true;
      } else {
        return null;
      }
      return (
        <Group
          searchQuery={this.state.searchQuery}
          group={group}
          // @ts-ignore
          user={this.props.data.initialData}
          openMenu={this.openMenu}
          closeMenu={this.closeMenu}
          modals={this.state.modals}
          showItems={false}
          menuAnchor={
            this.state.selectedGroup && this.state.selectedGroup.id === group.id
              ? this.state.menuAnchor
              : null
          }
          isAdmin={isAdmin}
          refetchGroups={() => this.props.data && this.props.data.refetch()}
          history={this.props.history}
          key={group.id}
        />
      );
    });
    return isAnAdmin ? groupsMap : null;
  }

  render() {
    if (this.props.data && this.props.data.error) {
      this.signMeOut();
    }
    if (!this.props.data || !this.props.data.initialData) {
      return null;
    }
    const { initialData }: any = this.props.data;

    const groups = initialData.groups;
    const groupItems = this.renderGroupItems(groups);

    const groupOwners = this.renderGroupOwners(groups);
    const modals = (
      <ModalProvider
        searchQuery={this.state.searchQuery}
        groups={this.props.data.initialData.groups}
        refetchGroups={() => this.props.data && this.props.data.refetch()}
        makeToast={this.props.makeToast}
        ref={c => {
          if (!this.state.modals) {
            this.setState({ modals: c });
          }
        }}
        history={this.props.history}
      />
    );
    return (
      <div className="container">
        {modals}
        <NavBar
          history={this.props.history}
          updateSearch={(searchQuery: string) => this.setState({ searchQuery })}
        />
        {this.props.data.initialData.name ? (
          <div className="home-content content">
            <div className="dashboard-header">
              <div className="dashboard-title">Your Libraries</div>
              <div className="request-something-else">
                <Button
                  color="primary"
                  onClick={() => this.props.history.push("/request")}
                  style={{
                    fontSize: "15px",
                    flex: 1
                  }}
                >
                  <b>Request Something Else</b>
                </Button>
              </div>
            </div>

            {initialData.groups.length > 0 ? (
              <Tabs>
                <TabList>
                  <Tab>Items</Tab>
                  {groupOwners && <Tab>Owners</Tab>}
                </TabList>

                <TabPanel>{groupItems}</TabPanel>
                {groupOwners && <TabPanel>{groupOwners}</TabPanel>}
              </Tabs>
            ) : (
              <Empty
                message={
                  <span>
                    You currently aren't a part of any groups.
                    <Button
                      color="primary"
                      onClick={this.state.modals.openCreateGroupModal}
                      style={{
                        padding: "0px",
                        minWidth: "0px",
                        marginLeft: "3px"
                      }}
                    >
                      Create
                    </Button>{" "}
                    a new group to get started!
                  </span>
                }
              />
            )}
          </div>
        ) : (
          <div className="content">
            <FillProfile
              history={this.props.history}
              // @ts-ignore
              reload={() => this.props.data.refetch()}
            />
          </div>
        )}
      </div>
    );
  }
}
