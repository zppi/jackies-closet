import React from "react";
import gql from "graphql-tag";
import { Mutation } from "react-apollo";
import { Button } from "@material-ui/core";
import { MdArrowBack } from "react-icons/md";

import { signIn, getSessionToken } from "../../services/AuthService";
import "./styles/LoginFlow.scss";
import "./styles/Home.scss";
import {
  VerifyTokenMutation,
  VerifyTokenMutationVariables
} from "./__generated__/VerifyTokenMutation";
import {
  LoginMutation,
  LoginMutationVariables
} from "./__generated__/LoginMutation";

const VERIFY_TOKEN = gql`
  mutation VerifyTokenMutation($verificationToken: String!) {
    verifyToken(verificationToken: $verificationToken) {
      sessionToken
    }
  }
`;
const SEND_LOGIN_EMAIL = gql`
  mutation LoginMutation($email: String!) {
    sendLoginEmail(email: $email) {
      email
    }
  }
`;

interface Props {
  history: any;
  match: any;
}

interface State {
  errorMessage: string;
}

class Verify extends React.Component<Props, State> {
  state: State = { errorMessage: "" };
  componentDidMount() {
    this.setState({
      errorMessage: ""
    });
    getSessionToken().then((token: any) => {
      if (token) {
        this.props.history.push("/home");
      }
    });
  }

  onSubmitCompleted = async (data: any) => {
    await signIn(data.verifyToken.sessionToken);
    this.props.history.push("/home");
  };

  onSubmitError = (error: any) => {
    this.setState({ errorMessage: error.graphQLErrors[0].message });
  };

  render() {
    const { token, email } = this.props.match.params;
    return (
      <div className="container">
        <div className="content">
          <div className="welcome">
            <div className="title-container">
              <h1 className="title">Jackie's Closet</h1>
              <div className="image-container">
                <img className="image" src="/logo.png" alt="jc-logo" />
              </div>
            </div>
          </div>
          <div className="verify-container">
            <div>Great! We just sent a link to {email}! Check your email!</div>
            <div className="error-container">{this.state.errorMessage}</div>
            <Mutation<LoginMutation, LoginMutationVariables>
              mutation={SEND_LOGIN_EMAIL}
              variables={{
                email: email
              }}
            >
              {(logIn: any) => {
                return (
                  <Button
                    className="verify-button"
                    color="primary"
                    variant="contained"
                    onClick={logIn}
                  >
                    Resend verification email
                  </Button>
                );
              }}
            </Mutation>
            <Button
              className="verify-button"
              color="primary"
              onClick={() => this.props.history.push("/home")}
            >
              <MdArrowBack color="primary" style={{ marginRight: "3px" }} />
              Use a different email
            </Button>
          </div>
          {token && (
            <Mutation<VerifyTokenMutation, VerifyTokenMutationVariables>
              mutation={VERIFY_TOKEN}
              variables={{
                verificationToken: token
              }}
              onCompleted={this.onSubmitCompleted}
              onError={this.onSubmitError}
            >
              {(verifyToken: any, { called }) => {
                return (
                  <CallVerify
                    verify={() => {
                      if (!called) {
                        verifyToken();
                      }
                    }}
                  />
                );
              }}
            </Mutation>
          )}
        </div>
      </div>
    );
  }
}
export default Verify;

interface CallVerifyProps {
  verify: any;
}

class CallVerify extends React.Component<CallVerifyProps> {
  componentDidMount() {
    this.props.verify();
  }

  render() {
    return null;
  }
}
