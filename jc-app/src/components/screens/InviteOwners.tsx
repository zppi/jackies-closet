import React from "react";
import gql from "graphql-tag";
import { Query, Mutation } from "react-apollo";
import { Button } from "@material-ui/core";
import { ReactMultiEmail, isEmail } from "react-multi-email";
import "react-multi-email/style.css";

import "../modals/styles/Modal.scss";
import {
  InviteOwnersMutation,
  InviteOwnersMutationVariables
} from "./__generated__/InviteOwnersMutation";
import { NavBar } from "..";

const INVITE_OWNERS = gql`
  mutation InviteOwnersMutation($id: String!, $emails: [String!]!) {
    addToGroup(id: $id, emails: $emails) {
      group {
        id
        name
      }
      users {
        id
        email
      }
    }
  }
`;
interface Props {
  history: any;
  match: any;
  makeToast: any;
}
interface State {
  emails: string[];
  errorMessage: string;
}

const GET_GROUP = gql`
  query InviteOwnersQuery($id: String!) {
    group(id: $id) {
      id
      name
    }
  }
`;
export default class InviteOwners extends React.Component<Props, State> {
  state: State = {
    emails: [],
    errorMessage: ""
  };

  componentDidMount() {
    this.setState({
      emails: [],
      errorMessage: ""
    });
    window.scrollTo(0, 1000);
  }

  onSubmitCompleted = (data: any) => {
    data.addToGroup.users.forEach((user: any) => {
      this.props.makeToast(
        "Invited " + user.email + " to " + data.addToGroup.group.name
      );
    });
    this.props.history.goBack();
  };

  onSubmitError = (error: any) => {
    this.setState({ errorMessage: error.graphQLErrors[0].message });
  };

  render() {
    const { groupId } = this.props.match.params;
    return (
      <div className="container">
        <NavBar history={this.props.history} />
        <Query query={GET_GROUP} variables={{ id: groupId }}>
          {({ loading, error, data }: any) => {
            if (loading) {
              return null;
            }
            const group = data.group;
            if (!group) {
              this.props.history.goBack();
              return null;
            }
            return (
              <div className="content modal-container">
                <h1 className="modal-title">Invite friends to {group.name}</h1>

                <Mutation<InviteOwnersMutation, InviteOwnersMutationVariables>
                  mutation={INVITE_OWNERS}
                  variables={{
                    id: group.id,
                    emails: this.state.emails
                  }}
                  onCompleted={this.onSubmitCompleted}
                  onError={this.onSubmitError}
                >
                  {(inviteOwners: any) => {
                    return (
                      <>
                        <div className="modal-content">
                          <ReactMultiEmail
                            placeholder="Your friends' emails"
                            emails={this.state.emails}
                            onChange={(_emails: string[]) => {
                              this.setState({ emails: _emails });
                            }}
                            validateEmail={email => {
                              return isEmail(email); // return boolean
                            }}
                            getLabel={(
                              email: string,
                              index: number,
                              removeEmail: (index: number) => void
                            ) => {
                              return (
                                <div data-tag key={index}>
                                  {email}
                                  <span
                                    data-tag-handle
                                    onClick={() => removeEmail(index)}
                                  >
                                    ×
                                  </span>
                                </div>
                              );
                            }}
                          />
                          <div className="modal-error-message">
                            {this.state.errorMessage}
                          </div>
                        </div>

                        <div className="modal-buttons-row">
                          <Button
                            color="primary"
                            onClick={() => this.props.history.goBack()}
                            className="modal-button"
                          >
                            Cancel
                          </Button>
                          <Button
                            color="primary"
                            variant="contained"
                            onClick={inviteOwners}
                            className="modal-button"
                          >
                            Send Invitation
                          </Button>
                        </div>
                      </>
                    );
                  }}
                </Mutation>
              </div>
            );
          }}
        </Query>
      </div>
    );
  }
}
