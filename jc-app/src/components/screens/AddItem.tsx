import React from "react";
import gql from "graphql-tag";
import { Query, Mutation } from "react-apollo";
import { Button } from "@material-ui/core";
import Modal from "react-modal";

import "../modals/styles/Modal.scss";
import {
  CreateItemMutation,
  CreateItemMutationVariables
} from "./__generated__/CreateItemMutation";

import { EditItemSpecifics, NavBar } from "..";

const CREATE_ITEM = gql`
  mutation CreateItemMutation(
    $name: String!
    $groupId: String!
    $category: String
    $isShared: Boolean
  ) {
    createItem(
      name: $name
      groupId: $groupId
      category: $category
      isShared: $isShared
    ) {
      id
      name
      category
      isShared
      group {
        id
        name
      }
    }
  }
`;
interface Props {
  match: any;
  history: any;
  makeToast: any;
}

interface State {
  itemName: string;
  category: string | null;
  isShared: boolean;
  errorMessage: string;
}

const GET_GROUP = gql`
  query AddItemQuery($id: String!) {
    group(id: $id) {
      id
      name
      categories
    }
  }
`;
export default class AddItem extends React.Component<Props, State> {
  state: State = {
    itemName: "",
    category: null,
    isShared: true,
    errorMessage: ""
  };
  componentDidMount() {
    this.setState({
      itemName: "",
      category: null,
      isShared: true,
      errorMessage: ""
    });
  }

  onSubmitCompleted = (data: any) => {
    this.props.makeToast(
      "Added " +
        data.createItem.name +
        " to " +
        (data.createItem.category ? data.createItem.category + " in " : "") +
        data.createItem.group.name
    );
    this.props.history.goBack();
  };

  onSubmitError = (error: any) => {
    this.setState({ errorMessage: error.graphQLErrors[0].message });
  };

  handleSharedChange = (event: any) => {
    this.setState({ isShared: event.target.checked });
  };

  render() {
    const { groupId } = this.props.match.params;

    return (
      <div className="container">
        <NavBar history={this.props.history} />
        <Query query={GET_GROUP} variables={{ id: groupId }}>
          {({ loading, error, data }: any) => {
            if (loading) {
              return null;
            }
            const group = data.group;
            if (!group) {
              this.props.history.goBack();
              return null;
            }
            const item: any = {
              name: this.state.itemName,
              category: this.state.category,
              isShared: this.state.isShared,
              group: group
            };
            return (
              <div className="content modal-container">
                <h1 className="modal-title">Add to {group.name}</h1>

                <Mutation<CreateItemMutation, CreateItemMutationVariables>
                  mutation={CREATE_ITEM}
                  variables={{
                    name: this.state.itemName,
                    groupId: group.id,
                    category: this.state.category,
                    isShared: this.state.isShared
                  }}
                  onCompleted={this.onSubmitCompleted}
                  onError={this.onSubmitError}
                >
                  {(createItem: any) => {
                    return (
                      <div className="modal-container">
                        <div className="modal-content">
                          <EditItemSpecifics
                            item={item}
                            updateItem={(item: any) =>
                              this.setState({
                                itemName: item.name,
                                isShared: item.isShared,
                                category: item.category
                              })
                            }
                          />

                          {this.state.errorMessage && (
                            <div className="modal-error-message">
                              {this.state.errorMessage}
                            </div>
                          )}
                        </div>

                        <div className="modal-buttons-row">
                          <Button
                            color="primary"
                            onClick={() => this.props.history.goBack()}
                            className="modal-button"
                          >
                            Cancel
                          </Button>
                          <Button
                            color="primary"
                            variant="contained"
                            onClick={createItem}
                            className="modal-button"
                          >
                            Add
                          </Button>
                        </div>
                      </div>
                    );
                  }}
                </Mutation>
              </div>
            );
          }}
        </Query>
      </div>
    );
  }
}

Modal.setAppElement("#root");
