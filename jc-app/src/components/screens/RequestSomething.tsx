import React from "react";
import gql from "graphql-tag";
import { Query, Mutation } from "react-apollo";
import { Button, TextField } from "@material-ui/core";
import Checkbox from "@material-ui/core/Checkbox";

import "../modals/styles/Modal.scss";
import "../modals/styles/RequestModal.scss";
import { InitialDataQuery_initialData_groups } from "../screens/__generated__/InitialDataQuery";
import {
  RequestSomethingMutation,
  RequestSomethingMutationVariables
} from "./__generated__/RequestSomethingMutation";
import { NavBar } from "..";

const REQUEST_SOMETHING = gql`
  mutation RequestSomethingMutation(
    $itemName: String!
    $message: String!
    $groupIds: [String!]!
  ) {
    requestSomething(
      itemName: $itemName
      message: $message
      groupIds: $groupIds
    ) {
      successful
    }
  }
`;

const GET_GROUPS = gql`
  query RequestSomethingQuery {
    initialData {
      id
      groups {
        name
        id
      }
    }
  }
`;

interface Props {
  history: any;
  match: any;
}

interface State {
  itemName: string;
  checkedGroupIds: string[];
  message: string;
}

export default class RequestSomething extends React.Component<Props, State> {
  state: State = {
    itemName: "",
    checkedGroupIds: [],
    message: ""
  };

  componentDidMount() {
    this.setState({
      itemName: "",
      checkedGroupIds: [],
      message: ""
    });
  }

  onSubmitCompleted = (data: any) => {
    this.props.history.goBack();
  };

  onSubmitError = (error: any) => {
    console.log(error);
  };

  updateItemName = (itemName: string) => {
    this.setState({ itemName });
  };

  updateMessage = (message: string) => {
    this.setState({ message });
  };

  convertDate(date: Date): number {
    const newDate = new Date(
      date.getTime() + date.getTimezoneOffset() * 60 * 1000
    );
    return newDate.getTime() / 1000;
  }

  changeGroupChecked = (group: InitialDataQuery_initialData_groups) => {
    let checkedGroupIds = this.state.checkedGroupIds;
    if (checkedGroupIds.includes(group.id)) {
      checkedGroupIds = checkedGroupIds.filter(function(value) {
        return value !== group.id;
      });
    } else {
      checkedGroupIds.push(group.id);
    }
    this.setState({ checkedGroupIds });
  };

  renderGroupChecks(groups: InitialDataQuery_initialData_groups[]) {
    return (
      <div
        style={{
          display: "flex",
          flexDirection: "column",
          alignItems: "flex-start",
          width: "100%"
        }}
        className="section-container"
      >
        <h3 className="section-title">Select Groups</h3>
        {groups.map(group => {
          return (
            <div className="checkbox-section" key={group.id}>
              <Checkbox
                checked={this.state.checkedGroupIds.includes(group.id)}
                onChange={() => this.changeGroupChecked(group)}
                color="primary"
              />
              <span className="checkbox-prompt">{group.name}</span>
            </div>
          );
        })}
      </div>
    );
  }

  render() {
    const { itemName, message, checkedGroupIds } = this.state;
    return (
      <div className="container">
        <NavBar history={this.props.history} />
        <Query query={GET_GROUPS}>
          {({ loading, error, data }: any) => {
            if (loading) {
              return null;
            }

            if (!data.initialData) {
              this.props.history.goBack();
              return null;
            }
            const groups: any = data.initialData.groups;
            const groupChecks = this.renderGroupChecks(groups);
            return (
              <div className="content modal-container">
                <span className="modal-title">Request Something Else</span>
                <div className="modal-content" style={{ alignItems: "left" }}>
                  {groupChecks}
                  <div style={{ width: "100%" }}>
                    <TextField
                      id="item-name"
                      label="Item name"
                      placeholder="Sleeping bag"
                      multiline={false}
                      variant="outlined"
                      value={itemName}
                      onChange={event =>
                        this.updateItemName(event.target.value)
                      }
                      className="item-name"
                      style={{ width: "100%" }}
                    />
                  </div>
                  <div style={{ margin: "10px 0px", width: "100%" }}>
                    <TextField
                      id="message"
                      label="Message"
                      placeholder="My dear friends..."
                      multiline={true}
                      variant="outlined"
                      value={message}
                      onChange={event => this.updateMessage(event.target.value)}
                      className="note"
                      style={{ width: "100%" }}
                      inputProps={{ style: { minHeight: "40px" } }}
                    />
                  </div>
                </div>

                <Mutation<
                  RequestSomethingMutation,
                  RequestSomethingMutationVariables
                >
                  mutation={REQUEST_SOMETHING}
                  variables={{
                    itemName: itemName,
                    message: message,
                    groupIds: checkedGroupIds
                  }}
                  onCompleted={this.onSubmitCompleted}
                  onError={this.onSubmitError}
                >
                  {(requestSomething: any) => {
                    return (
                      <div className="modal-buttons-row">
                        <Button
                          color="primary"
                          onClick={() => this.props.history.goBack()}
                          className="modal-button"
                        >
                          Cancel
                        </Button>
                        <Button
                          color="primary"
                          variant="contained"
                          onClick={requestSomething}
                          className="modal-button"
                          disabled={
                            this.state.checkedGroupIds.length === 0 ||
                            this.state.itemName === ""
                          }
                        >
                          Send
                        </Button>
                      </div>
                    );
                  }}
                </Mutation>
              </div>
            );
          }}
        </Query>
      </div>
    );
  }
}
