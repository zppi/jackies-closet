/* tslint:disable */
/* eslint-disable */
// This file was automatically generated and should not be edited.

// ====================================================
// GraphQL query operation: RequestSomethingQuery
// ====================================================

export interface RequestSomethingQuery_initialData_groups {
  __typename: "Group";
  name: string;
  id: string;
}

export interface RequestSomethingQuery_initialData {
  __typename: "User";
  id: string;
  groups: RequestSomethingQuery_initialData_groups[];
}

export interface RequestSomethingQuery {
  initialData: RequestSomethingQuery_initialData | null;
}
