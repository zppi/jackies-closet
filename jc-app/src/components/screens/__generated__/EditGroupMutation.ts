/* tslint:disable */
/* eslint-disable */
// This file was automatically generated and should not be edited.

// ====================================================
// GraphQL mutation operation: EditGroupMutation
// ====================================================

export interface EditGroupMutation_editGroup {
  __typename: "Group";
  id: string;
  name: string;
}

export interface EditGroupMutation {
  editGroup: EditGroupMutation_editGroup;
}

export interface EditGroupMutationVariables {
  id: string;
  name: string;
}
