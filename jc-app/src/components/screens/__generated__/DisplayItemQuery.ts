/* tslint:disable */
/* eslint-disable */
// This file was automatically generated and should not be edited.

// ====================================================
// GraphQL query operation: DisplayItemQuery
// ====================================================

export interface DisplayItemQuery_item_group {
  __typename: "Group";
  id: string;
  name: string;
  categories: string[];
}

export interface DisplayItemQuery_item_user {
  __typename: "User";
  id: string;
  name: string | null;
}

export interface DisplayItemQuery_item {
  __typename: "Item";
  id: string;
  name: string;
  isShared: boolean;
  category: string | null;
  group: DisplayItemQuery_item_group;
  user: DisplayItemQuery_item_user;
}

export interface DisplayItemQuery_initialData {
  __typename: "User";
  id: string;
}

export interface DisplayItemQuery {
  item: DisplayItemQuery_item | null;
  initialData: DisplayItemQuery_initialData | null;
}

export interface DisplayItemQueryVariables {
  id: string;
}
