/* tslint:disable */
/* eslint-disable */
// This file was automatically generated and should not be edited.

// ====================================================
// GraphQL mutation operation: InviteOwnersMutation
// ====================================================

export interface InviteOwnersMutation_addToGroup_group {
  __typename: "Group";
  id: string;
  name: string;
}

export interface InviteOwnersMutation_addToGroup_users {
  __typename: "User";
  id: string;
  email: string;
}

export interface InviteOwnersMutation_addToGroup {
  __typename: "GroupUsers";
  group: InviteOwnersMutation_addToGroup_group;
  users: InviteOwnersMutation_addToGroup_users[];
}

export interface InviteOwnersMutation {
  addToGroup: InviteOwnersMutation_addToGroup;
}

export interface InviteOwnersMutationVariables {
  id: string;
  emails: string[];
}
