/* tslint:disable */
/* eslint-disable */
// This file was automatically generated and should not be edited.

// ====================================================
// GraphQL mutation operation: VerifyTokenMutation
// ====================================================

export interface VerifyTokenMutation_verifyToken {
  __typename: "VerifyTokenPayload";
  sessionToken: string;
}

export interface VerifyTokenMutation {
  verifyToken: VerifyTokenMutation_verifyToken;
}

export interface VerifyTokenMutationVariables {
  verificationToken: string;
}
