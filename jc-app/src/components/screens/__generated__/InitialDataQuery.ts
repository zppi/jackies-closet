/* tslint:disable */
/* eslint-disable */
// This file was automatically generated and should not be edited.

// ====================================================
// GraphQL query operation: InitialDataQuery
// ====================================================

export interface InitialDataQuery_initialData_groups_items_user {
  __typename: "User";
  name: string | null;
  id: string;
}

export interface InitialDataQuery_initialData_groups_items_group {
  __typename: "Group";
  id: string;
  categories: string[];
  name: string;
}

export interface InitialDataQuery_initialData_groups_items {
  __typename: "Item";
  name: string;
  id: string;
  user: InitialDataQuery_initialData_groups_items_user;
  category: string | null;
  group: InitialDataQuery_initialData_groups_items_group;
  isShared: boolean;
}

export interface InitialDataQuery_initialData_groups_users_items_group {
  __typename: "Group";
  id: string;
}

export interface InitialDataQuery_initialData_groups_users_items {
  __typename: "Item";
  name: string;
  id: string;
  group: InitialDataQuery_initialData_groups_users_items_group;
}

export interface InitialDataQuery_initialData_groups_users {
  __typename: "User";
  name: string | null;
  email: string;
  id: string;
  items: InitialDataQuery_initialData_groups_users_items[];
}

export interface InitialDataQuery_initialData_groups {
  __typename: "Group";
  name: string;
  id: string;
  items: InitialDataQuery_initialData_groups_items[];
  users: InitialDataQuery_initialData_groups_users[];
  adminIds: string[];
  categories: string[];
}

export interface InitialDataQuery_initialData {
  __typename: "User";
  id: string;
  name: string | null;
  email: string;
  groups: InitialDataQuery_initialData_groups[];
}

export interface InitialDataQuery {
  initialData: InitialDataQuery_initialData | null;
}
