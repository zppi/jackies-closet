/* tslint:disable */
/* eslint-disable */
// This file was automatically generated and should not be edited.

// ====================================================
// GraphQL mutation operation: RequestSomethingMutation
// ====================================================

export interface RequestSomethingMutation_requestSomething {
  __typename: "RequestSomethingPayload";
  successful: string;
}

export interface RequestSomethingMutation {
  requestSomething: RequestSomethingMutation_requestSomething;
}

export interface RequestSomethingMutationVariables {
  itemName: string;
  message: string;
  groupIds: string[];
}
