/* tslint:disable */
/* eslint-disable */
// This file was automatically generated and should not be edited.

// ====================================================
// GraphQL mutation operation: CreateItemMutation
// ====================================================

export interface CreateItemMutation_createItem_group {
  __typename: "Group";
  id: string;
  name: string;
}

export interface CreateItemMutation_createItem {
  __typename: "Item";
  id: string;
  name: string;
  category: string | null;
  isShared: boolean;
  group: CreateItemMutation_createItem_group;
}

export interface CreateItemMutation {
  createItem: CreateItemMutation_createItem;
}

export interface CreateItemMutationVariables {
  name: string;
  groupId: string;
  category?: string | null;
  isShared?: boolean | null;
}
