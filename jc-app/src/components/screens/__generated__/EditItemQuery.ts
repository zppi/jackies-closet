/* tslint:disable */
/* eslint-disable */
// This file was automatically generated and should not be edited.

// ====================================================
// GraphQL query operation: EditItemQuery
// ====================================================

export interface EditItemQuery_item_group {
  __typename: "Group";
  id: string;
  name: string;
  categories: string[];
}

export interface EditItemQuery_item {
  __typename: "Item";
  id: string;
  name: string;
  isShared: boolean;
  category: string | null;
  group: EditItemQuery_item_group;
}

export interface EditItemQuery {
  item: EditItemQuery_item | null;
}

export interface EditItemQueryVariables {
  id: string;
}
