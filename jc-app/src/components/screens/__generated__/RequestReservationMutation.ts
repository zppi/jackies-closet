/* tslint:disable */
/* eslint-disable */
// This file was automatically generated and should not be edited.

// ====================================================
// GraphQL mutation operation: RequestReservationMutation
// ====================================================

export interface RequestReservationMutation_requestReservation {
  __typename: "RequestReservationPayload";
  successful: string;
}

export interface RequestReservationMutation {
  requestReservation: RequestReservationMutation_requestReservation;
}

export interface RequestReservationMutationVariables {
  itemId: string;
  startDate: number;
  endDate?: number | null;
  note: string;
}
