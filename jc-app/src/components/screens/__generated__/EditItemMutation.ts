/* tslint:disable */
/* eslint-disable */
// This file was automatically generated and should not be edited.

// ====================================================
// GraphQL mutation operation: EditItemMutation
// ====================================================

export interface EditItemMutation_editItem {
  __typename: "Item";
  id: string;
  name: string;
  category: string | null;
  isShared: boolean;
}

export interface EditItemMutation {
  editItem: EditItemMutation_editItem;
}

export interface EditItemMutationVariables {
  id: string;
  name: string;
  category?: string | null;
  isShared?: boolean | null;
}
