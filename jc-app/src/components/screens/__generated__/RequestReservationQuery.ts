/* tslint:disable */
/* eslint-disable */
// This file was automatically generated and should not be edited.

// ====================================================
// GraphQL query operation: RequestReservationQuery
// ====================================================

export interface RequestReservationQuery_item_group {
  __typename: "Group";
  id: string;
  name: string;
}

export interface RequestReservationQuery_item {
  __typename: "Item";
  id: string;
  name: string;
  isShared: boolean;
  category: string | null;
  group: RequestReservationQuery_item_group;
}

export interface RequestReservationQuery {
  item: RequestReservationQuery_item | null;
}

export interface RequestReservationQueryVariables {
  id: string;
}
