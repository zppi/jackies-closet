/* tslint:disable */
/* eslint-disable */
// This file was automatically generated and should not be edited.

// ====================================================
// GraphQL query operation: EditGroupQuery
// ====================================================

export interface EditGroupQuery_group {
  __typename: "Group";
  id: string;
  name: string;
  categories: string[];
}

export interface EditGroupQuery {
  group: EditGroupQuery_group | null;
}

export interface EditGroupQueryVariables {
  id: string;
}
