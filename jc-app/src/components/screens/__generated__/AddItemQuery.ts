/* tslint:disable */
/* eslint-disable */
// This file was automatically generated and should not be edited.

// ====================================================
// GraphQL query operation: AddItemQuery
// ====================================================

export interface AddItemQuery_group {
  __typename: "Group";
  id: string;
  name: string;
  categories: string[];
}

export interface AddItemQuery {
  group: AddItemQuery_group | null;
}

export interface AddItemQueryVariables {
  id: string;
}
