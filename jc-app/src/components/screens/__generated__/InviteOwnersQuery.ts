/* tslint:disable */
/* eslint-disable */
// This file was automatically generated and should not be edited.

// ====================================================
// GraphQL query operation: InviteOwnersQuery
// ====================================================

export interface InviteOwnersQuery_group {
  __typename: "Group";
  id: string;
  name: string;
}

export interface InviteOwnersQuery {
  group: InviteOwnersQuery_group | null;
}

export interface InviteOwnersQueryVariables {
  id: string;
}
