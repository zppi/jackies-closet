/* tslint:disable */
/* eslint-disable */
// This file was automatically generated and should not be edited.

// ====================================================
// GraphQL mutation operation: LoginMutation
// ====================================================

export interface LoginMutation_sendLoginEmail {
  __typename: "SendLoginEmailPayload";
  email: string;
}

export interface LoginMutation {
  sendLoginEmail: LoginMutation_sendLoginEmail;
}

export interface LoginMutationVariables {
  email: string;
}
