import React from "react";
import gql from "graphql-tag";
import { Mutation } from "react-apollo";
import { Button, TextField } from "@material-ui/core";
import { MdArrowForward } from "react-icons/md";

import { getSessionToken } from "../../services/AuthService";
import "./styles/LoginFlow.scss";
import {
  LoginMutation,
  LoginMutationVariables
} from "./__generated__/LoginMutation";

const SEND_LOGIN_EMAIL = gql`
  mutation LoginMutation($email: String!) {
    sendLoginEmail(email: $email) {
      email
    }
  }
`;

interface Props {
  history: any;
}

interface State {
  email: string;
  emailSent: boolean;
  errorMessage: string;
}

class Login extends React.Component<Props, State> {
  state: State = { email: "", emailSent: false, errorMessage: "" };
  componentDidMount() {
    this.setState({
      email: "",
      errorMessage: "",
      emailSent: false
    });
    getSessionToken().then((token: any) => {
      if (token) {
        this.props.history.push("/home");
      }
    });
  }

  onSubmitCompleted = (data: any) => {
    this.props.history.push("/verify/" + data.sendLoginEmail.email);
  };

  onSubmitError = (error: any) => {
    this.setState({ errorMessage: error.graphQLErrors[0].message });
  };

  render() {
    return (
      <div className="container">
        <div className="content">
          <div className="welcome">
            <div className="title-container">
              <h1 className="title">Jackie's Closet</h1>
              <div className="image-container">
                <img className="image" src="logo.png" alt="jc-logo" />
              </div>
            </div>
            <div>
              <h1>Sign in or sign up</h1>
            </div>
          </div>
          <div className="login-container">
            <div className="instructions">Enter your email!</div>
            <Mutation<LoginMutation, LoginMutationVariables>
              mutation={SEND_LOGIN_EMAIL}
              variables={{
                email: this.state.email
              }}
              onCompleted={this.onSubmitCompleted}
              onError={this.onSubmitError}
            >
              {(logIn: any) => {
                return (
                  <div>
                    <div className="textfield-container">
                      <TextField
                        className="textfield"
                        label="Email"
                        variant="outlined"
                        value={this.state.email}
                        onChange={event =>
                          this.setState({ email: event.target.value })
                        }
                        onKeyPress={(e: any) => e.key === "Enter" && logIn()}
                        inputProps={{
                          autoCapitalize: "none"
                        }}
                      />
                    </div>
                    <div className="error-container">
                      {this.state.errorMessage !== "" &&
                        this.state.errorMessage}
                    </div>
                    <div className="buttons-row">
                      <Button
                        color="primary"
                        variant="outlined"
                        onClick={logIn}
                      >
                        Send verification email
                        <MdArrowForward color="primary" />
                      </Button>
                    </div>
                  </div>
                );
              }}
            </Mutation>
          </div>
        </div>
      </div>
    );
  }
}
export default Login;
