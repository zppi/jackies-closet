import React from "react";
import gql from "graphql-tag";
import { Query, Mutation } from "react-apollo";
import { Button, TextField } from "@material-ui/core";

import "../modals/styles/Modal.scss";
import {
  EditGroupMutation,
  EditGroupMutationVariables
} from "./__generated__/EditGroupMutation";
import { NavBar } from "..";

const EDIT_GROUP = gql`
  mutation EditGroupMutation($id: String!, $name: String!) {
    editGroup(id: $id, name: $name) {
      id
      name
    }
  }
`;
interface Props {
  history: any;
  match: any;
}
interface State {
  groupName: string | null;
  errorMessage: string;
}

const GET_GROUP = gql`
  query EditGroupQuery($id: String!) {
    group(id: $id) {
      id
      name
      categories
    }
  }
`;
export default class EditGroup extends React.Component<Props, State> {
  state: State = {
    groupName: null,
    errorMessage: ""
  };

  componentDidMount() {
    this.setState({ groupName: null, errorMessage: "" });
  }

  onSubmitCompleted = (data: any) => {
    this.props.history.goBack();
  };

  onSubmitError = (error: any) => {
    this.setState({ errorMessage: error.graphQLErrors[0].message });
  };

  render() {
    const { groupId } = this.props.match.params;
    return (
      <div className="container">
        <NavBar history={this.props.history} />
        <Query query={GET_GROUP} variables={{ id: groupId }}>
          {({ loading, error, data }: any) => {
            if (loading) {
              return null;
            }
            const group = data.group;
            if (!group) {
              this.props.history.goBack();
              return null;
            }
            return (
              <div className="content modal-container">
                <h1 className="modal-title">Edit {group.name}</h1>

                <Mutation<EditGroupMutation, EditGroupMutationVariables>
                  mutation={EDIT_GROUP}
                  variables={{
                    id: group.id,
                    name: this.state.groupName || group.name
                  }}
                  onCompleted={this.onSubmitCompleted}
                  onError={this.onSubmitError}
                >
                  {(editGroup: any) => {
                    return (
                      <div>
                        <div className="modal-content">
                          <TextField
                            className="textfield"
                            label="Group name"
                            variant="outlined"
                            value={
                              this.state.groupName !== null
                                ? this.state.groupName
                                : group.name
                            }
                            onChange={event =>
                              this.setState({ groupName: event.target.value })
                            }
                            onKeyPress={(e: any) =>
                              e.key === "Enter" && editGroup()
                            }
                          />
                          <div className="modal-error-message">
                            {this.state.errorMessage}
                          </div>
                        </div>

                        <div className="modal-buttons-row">
                          <Button
                            color="primary"
                            onClick={() => this.props.history.goBack()}
                            className="modal-button"
                          >
                            Cancel
                          </Button>
                          <Button
                            color="primary"
                            variant="contained"
                            onClick={editGroup}
                            className="modal-button"
                          >
                            Save
                          </Button>
                        </div>
                      </div>
                    );
                  }}
                </Mutation>
              </div>
            );
          }}
        </Query>
      </div>
    );
  }
}
