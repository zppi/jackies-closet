import React from "react";
import { MdSearch } from "react-icons/md";
import { InputBase } from "@material-ui/core";

import "./styles/SearchBar.scss";

interface Props {
  onChange: any;
  placeHolder: string;
}

export default class SearchBar extends React.Component<Props> {
  render() {
    return (
      <div className="search">
        <div className="search-icon-container">
          <MdSearch className="search-icon" color="gray" />
        </div>
        <InputBase
          onChange={this.props.onChange}
          placeholder={this.props.placeHolder}
          className="input-root"
          inputProps={{ "aria-label": "Search" }}
        />
      </div>
    );
  }
}
