import React from "react";
import { Button, Menu, MenuItem } from "@material-ui/core";
import {
  MdSettings,
  MdArrowDropUp,
  MdArrowDropDown,
  MdDelete,
  MdSupervisorAccount
} from "react-icons/md";

import {
  InitialDataQuery_initialData,
  InitialDataQuery_initialData_groups,
  InitialDataQuery_initialData_groups_items,
  InitialDataQuery_initialData_groups_users
} from "./screens/__generated__/InitialDataQuery";
import { Empty, Category } from ".";

import "./styles/Group.scss";

interface Props {
  searchQuery: string;
  group: InitialDataQuery_initialData_groups;
  user: InitialDataQuery_initialData;
  openMenu: any;
  closeMenu: any;
  modals: any;
  showItems: boolean;
  menuAnchor: any;
  isAdmin: boolean;
  refetchGroups: any;
  history: any;
}

interface State {
  selectedUser: InitialDataQuery_initialData_groups_users | null;
  ownerModalIsOpen: boolean;
}

const CATEGORY_COLORS = new Map([
  ["kitchen", ["140", "0", "250"]],
  ["childcare", ["0", "0", "255"]],
  ["outdoor & camping", ["40", "180", "50"]],
  ["festivity", ["255", "0", "255"]],
  ["emergency preparedness", ["255", "0", "0"]],
  ["tools", ["0", "0", "0"]]
]);

// @ts-ignore
export default class Group extends React.Component<Props, State> {
  state: State = { selectedUser: null, ownerModalIsOpen: false };

  componentDidMount() {
    this.setState({ selectedUser: null, ownerModalIsOpen: false });
  }

  userIsAdmin(user: InitialDataQuery_initialData_groups_users): boolean {
    return this.props.group.adminIds.includes(user.id);
  }

  openOwnerModal(user: InitialDataQuery_initialData_groups_users) {
    this.props.closeMenu();
    this.props.modals.openOwnerModal(
      user,
      this.props.group,
      user.id === this.props.user.id,
      this.userIsAdmin(user)
    );
  }

  renderUserItemNames(
    user: InitialDataQuery_initialData_groups_users,
    groupID: string
  ): string {
    let items = "";
    user.items.forEach(item => {
      if (item.group.id !== groupID) {
        return;
      }
      if (items !== "") {
        items += ", ";
      }
      items += item.name;
    });
    if (items.length > 30) {
      items = items.substring(0, 27) + "...";
    }
    return items;
  }

  itemMatches(item: InitialDataQuery_initialData_groups_items, query: string) {
    return (
      item.name.toLowerCase().includes(query.toLowerCase()) ||
      (item.user.name &&
        item.user.name.toLowerCase().includes(query.toLowerCase())) ||
      (item.category &&
        item.category.toLowerCase().includes(query.toLowerCase()))
    );
  }

  filterItems(
    items: InitialDataQuery_initialData_groups_items[],
    categories: string[]
  ) {
    let matchingItemsCategories = new Map();
    categories.forEach(category => {
      matchingItemsCategories.set(category, []);
    });
    matchingItemsCategories.set("other", []);
    items.forEach((item: InitialDataQuery_initialData_groups_items) => {
      if (this.itemMatches(item, this.props.searchQuery)) {
        const itemCategory = item.category || "other";
        let itemArray = matchingItemsCategories.get(itemCategory);
        itemArray.push(item);
        matchingItemsCategories.set(itemCategory, itemArray);
      }
    });
    items.sort((a: any) => {
      if (a.user.id === this.props.user.id) {
        return -1;
      }
      return 0;
    });
    return matchingItemsCategories;
  }

  filterOwners(
    owners: InitialDataQuery_initialData_groups_users[]
  ): InitialDataQuery_initialData_groups_users[] {
    let matchingOwners: InitialDataQuery_initialData_groups_users[] = [];
    owners.forEach((owner: InitialDataQuery_initialData_groups_users) => {
      if (
        (owner.name &&
          owner.name
            .toLowerCase()
            .includes(this.props.searchQuery.toLowerCase())) ||
        owner.email.toLowerCase().includes(this.props.searchQuery.toLowerCase())
      ) {
        matchingOwners.push(owner);
      }
    });
    matchingOwners.sort((a: any, b: any) => {
      if (a.id === this.props.user.id) {
        return -1;
      }
      if (this.userIsAdmin(a) && !this.userIsAdmin(b)) {
        return -1;
      }
      if (a.name && !b.name) {
        return -1;
      }
      return 0;
    });
    return matchingOwners;
  }

  renderGroupTitleRow(group: InitialDataQuery_initialData_groups) {
    const gearButton = (
      <Button
        className="gear-button"
        onClick={(event: any) => this.props.openMenu(event, group)}
      >
        <MdSettings color="#3F51B5" className="icon" />
        {this.props.menuAnchor ? (
          <MdArrowDropUp
            color="#3F51B5"
            className="icon"
            style={{ marginLeft: "-5px" }}
          />
        ) : (
          <MdArrowDropDown
            color="#3F51B5"
            className="icon"
            style={{ marginLeft: "-5px" }}
          />
        )}
      </Button>
    );
    return (
      <div className="group-title-row">
        <div className="group-title">
          {group.name}
          {this.props.isAdmin && gearButton}
          <Menu
            id="nav-menu"
            anchorEl={this.props.menuAnchor}
            getContentAnchorEl={null}
            anchorOrigin={{
              vertical: "bottom",
              horizontal: "left"
            }}
            keepMounted
            open={Boolean(this.props.menuAnchor)}
            onClose={() => this.props.closeMenu()}
          >
            <MenuItem
              onClick={() => {
                this.props.closeMenu();
                this.props.history.push("/edit-group/" + group.id);
              }}
            >
              Edit Group
            </MenuItem>
            <MenuItem
              onClick={() => {
                this.props.closeMenu();
                this.props.history.push("/invite/" + group.id);
              }}
            >
              Invite someone
            </MenuItem>
          </Menu>
        </div>
        {this.props.showItems && (
          <Button
            color="primary"
            variant="contained"
            onClick={() => this.props.history.push("/add-item/" + group.id)}
            className="add-item-button"
          >
            <div>
              <b>Add an Item</b>
            </div>
          </Button>
        )}
      </div>
    );
  }

  renderGroupItems(group: InitialDataQuery_initialData_groups) {
    const filteredItemsCategories = this.filterItems(
      group.items,
      group.categories
    );
    return (
      <div>
        {group.items.length > 0 ? (
          <div className="categories-container">
            {group.categories.map((category: string) => (
              <div className="category-container" key={category}>
                <Category
                  category={category}
                  items={filteredItemsCategories.get(category)}
                  user={this.props.user}
                  openDeleteItemModal={this.props.modals.openDeleteItemModal}
                  isAdmin={this.props.isAdmin}
                  color={CATEGORY_COLORS.get(category) || ["159", "159", "159"]}
                  history={this.props.history}
                  key={category}
                />
              </div>
            ))}
            <div className="category-container" key={"other"}>
              <Category
                category={"other"}
                items={filteredItemsCategories.get("other")}
                user={this.props.user}
                openDeleteItemModal={this.props.modals.openDeleteItemModal}
                isAdmin={this.props.isAdmin}
                color={["159", "159", "159"]}
                history={this.props.history}
              />
            </div>
          </div>
        ) : (
          <Empty
            message={
              <span>
                This group does not currently contain any items. Why not{" "}
                <Button
                  color="primary"
                  onClick={() =>
                    this.props.history.push("/add-item/" + group.id)
                  }
                  style={{
                    padding: "0px",
                    minWidth: "0px"
                  }}
                >
                  Add
                </Button>{" "}
                the first item yourself?{" "}
              </span>
            }
          />
        )}
      </div>
    );
  }

  renderGroupOwners(group: InitialDataQuery_initialData_groups) {
    const filteredOwners = this.filterOwners(group.users);
    return (
      <div>
        {filteredOwners.length > 0 ? (
          <div className="table-container">
            {filteredOwners.map(
              (user: InitialDataQuery_initialData_groups_users) => {
                const userItems = this.renderUserItemNames(user, group.id);
                const itemsElement =
                  userItems !== "" ? (
                    <span>{userItems}</span>
                  ) : (
                    <span>No items</span>
                  );
                return (
                  <div className="owner-row" key={user.id}>
                    <Button
                      className="owner-container"
                      key={user.id}
                      onClick={() => this.openOwnerModal(user)}
                    >
                      <span>
                        <b>{user.name}</b>
                      </span>
                      <span className="owner-email">{user.email}</span>
                      {itemsElement}
                    </Button>
                    <div className="delete-button">
                      {this.userIsAdmin(user) ? (
                        <Button
                          color="primary"
                          onClick={() => this.openOwnerModal(user)}
                        >
                          <MdSupervisorAccount
                            color="primary"
                            className="icon"
                          />
                        </Button>
                      ) : (
                        <Button
                          color="secondary"
                          onClick={() =>
                            this.props.modals.openRemoveOwnerModal(user, group)
                          }
                        >
                          <MdDelete color="secondary" className="icon" />
                        </Button>
                      )}
                    </div>
                  </div>
                );
              }
            )}
          </div>
        ) : (
          <Empty message="No owners in this group match your query." />
        )}
      </div>
    );
  }

  render() {
    const group = this.props.group;
    if (!this.props.showItems && !this.props.isAdmin) {
      return null;
    }
    const groupTitleRow = this.renderGroupTitleRow(group);
    const groupItems = this.renderGroupItems(group);
    const groupOwners = this.renderGroupOwners(group);
    return (
      <div className="group-container">
        {groupTitleRow}
        {this.props.showItems ? groupItems : groupOwners}
      </div>
    );
  }
}
