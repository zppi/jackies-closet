import React from "react";
import gql from "graphql-tag";
import { Mutation } from "react-apollo";
import { Button, TextField } from "@material-ui/core";
import { MdArrowForward } from "react-icons/md";

import "./screens/styles/LoginFlow.scss";
import {
  FillProfileMutation,
  FillProfileMutationVariables
} from "./__generated__/FillProfileMutation";

const FILL_PROFILE = gql`
  mutation FillProfileMutation($name: String!) {
    editProfile(name: $name) {
      name
    }
  }
`;

interface Props {
  history: any;
  reload: any;
}

interface State {
  name: string;
  errorMessage: string;
}

export default class FillProfile extends React.Component<Props, State> {
  state: State = { name: "", errorMessage: "" };
  componentDidMount() {
    this.setState({
      name: "",
      errorMessage: ""
    });
  }

  onSubmitCompleted = async (data: any) => {
    this.props.reload();
  };

  onSubmitError = (error: any) => {
    this.setState({ errorMessage: error.graphQLErrors[0].message });
  };

  render() {
    return (
      <div className="profile-container">
        <h1>Last step!</h1>
        <Mutation<FillProfileMutation, FillProfileMutationVariables>
          mutation={FILL_PROFILE}
          variables={{
            name: this.state.name
          }}
          onCompleted={this.onSubmitCompleted}
          onError={this.onSubmitError}
        >
          {(fillProfile: any) => {
            return (
              <div>
                <div className="textfield-container">
                  <TextField
                    className="textfield"
                    label="Your Name"
                    variant="outlined"
                    value={this.state.name}
                    onChange={event =>
                      this.setState({ name: event.target.value })
                    }
                    onKeyPress={(e: any) =>
                      e.key === "Enter" &&
                      this.state.name.length >= 5 &&
                      fillProfile()
                    }
                    inputProps={{
                      autoCapitalize: "none"
                    }}
                  />
                </div>
                <div className="error-container">
                  {this.state.errorMessage !== "" && this.state.errorMessage}
                </div>
                <div className="buttons-row">
                  <Button
                    color="primary"
                    variant="outlined"
                    onClick={fillProfile}
                    disabled={this.state.name.length < 5}
                  >
                    Continue
                    <MdArrowForward color="primary" />
                  </Button>
                </div>
              </div>
            );
          }}
        </Mutation>
      </div>
    );
  }
}
