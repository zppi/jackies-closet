import React from "react";
import { Button } from "@material-ui/core";
import { MdDelete } from "react-icons/md";

import { InitialDataQuery_initialData_groups_items } from "./screens/__generated__/InitialDataQuery";

import "./styles/Item.scss";

interface Props {
  item: InitialDataQuery_initialData_groups_items;
  openDelete: any;
  canDelete: boolean;
  history: any;
}

export default class Item extends React.Component<Props> {
  render() {
    const { item, openDelete, canDelete } = this.props;
    return (
      <div className="item" key={item.id}>
        <Button
          className="item-touchable"
          onClick={() => {
            console.log(item);
            this.props.history.push("/item/" + item.id);
          }}
        >
          <div className="button-content">
            <span className="item-name">
              <b>{item.name}</b>
            </span>
            <span className="owner-name">{item.user.name}</span>
          </div>
        </Button>
        {canDelete && (
          <div className="delete-container">
            <Button
              className="delete-button"
              color="secondary"
              onClick={() => openDelete(item)}
            >
              <MdDelete color="secondary" className="icon" />
            </Button>
          </div>
        )}
      </div>
    );
  }
}
