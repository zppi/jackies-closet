import React from "react";
import { MdMenu } from "react-icons/md";
import { Button, Menu, MenuItem } from "@material-ui/core";

import "./styles/NavBar.scss";
import { signOut } from "../services/AuthService";
import { SearchBar } from ".";

interface Props {
  history: any;
  updateSearch?: any;
}

interface State {
  menuOpen: boolean;
  anchorEl: any;
}

export default class NavBar extends React.Component<Props, State> {
  state: State = { menuOpen: false, anchorEl: null };

  componentDidMount() {
    this.setState({ menuOpen: false, anchorEl: null });
  }

  openMenu = (event: any): void => {
    this.setState({ anchorEl: event.currentTarget });
  };

  closeMenu = (): void => {
    this.setState({ anchorEl: null });
  };

  signMeOut = () => {
    signOut();
    this.closeMenu();
    this.props.history.push("/login");
  };

  render() {
    return (
      <div className="heading">
        <div className="navbar-container">
          <div
            className="nav-title-container"
            onClick={() => {
              this.props.history.push("/home");
            }}
          >
            <h1 className="nav-title">Jackie's Closet</h1>
            <div className="image-container">
              <img className="image" src="/logo.png" alt="jc-logo" />
            </div>
          </div>
          {this.props.updateSearch && (
            <div className="search-elements display-desktop">
              <div className="search-bar">
                <SearchBar
                  placeHolder="Find item or owner..."
                  onChange={(event: any) =>
                    this.props.updateSearch(event.target.value)
                  }
                />
              </div>
            </div>
          )}
          <div className="menu-container">
            <Button
              className="menu-button"
              onClick={(event: any) => this.openMenu(event)}
            >
              <MdMenu className="menu-icon" color="white" />
            </Button>
          </div>
          <Menu
            id="nav-menu"
            anchorEl={this.state.anchorEl}
            getContentAnchorEl={null}
            anchorOrigin={{
              vertical: "bottom",
              horizontal: "left"
            }}
            keepMounted
            open={Boolean(this.state.anchorEl)}
            onClose={() => this.closeMenu()}
          >
            <MenuItem onClick={this.signMeOut}>Sign out</MenuItem>
          </Menu>
        </div>
        {this.props.updateSearch && (
          <div className="search-elements display-mobile">
            <div className="search-bar">
              <SearchBar
                placeHolder="Find item or owner..."
                onChange={(event: any) =>
                  this.props.updateSearch(event.target.value)
                }
              />
            </div>
          </div>
        )}
      </div>
    );
  }
}
