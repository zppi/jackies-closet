import React from "react";
import {
  InitialDataQuery_initialData,
  InitialDataQuery_initialData_groups_items
} from "./screens/__generated__/InitialDataQuery";
import { Item } from ".";

import "./styles/Category.scss";

interface Props {
  category: string;
  items: InitialDataQuery_initialData_groups_items[];
  user: InitialDataQuery_initialData;
  openDeleteItemModal: any;
  isAdmin: boolean;
  color: string[];
  history: any;
}

// @ts-ignore
export default class Category extends React.Component<Props> {
  render() {
    const { category, items, user, isAdmin, color } = this.props;
    const titleStyles = {
      color: "rgba(" + color[0] + ", " + color[1] + ", " + color[2] + ", 1)",
      border:
        "1px solid rgba(" +
        color[0] +
        ", " +
        color[1] +
        ", " +
        color[2] +
        ", " +
        "1)",
      backgroundColor:
        "rgba(" + color[0] + ", " + color[1] + ", " + color[2] + ", .05)"
    };

    if (items.length === 0) {
      return null;
    }

    return (
      <div className="category">
        <div style={titleStyles} className="category-title">
          {category}
        </div>
        {items.length > 0 && (
          <div className="items-container">
            {items.map((item: InitialDataQuery_initialData_groups_items) => (
              <div className="item-container" key={item.id}>
                <Item
                  item={item}
                  openDelete={this.props.openDeleteItemModal}
                  canDelete={user.id === item.user.id || isAdmin}
                  history={this.props.history}
                />
              </div>
            ))}
          </div>
        )}
      </div>
    );
  }
}
