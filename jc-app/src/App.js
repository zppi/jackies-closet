import React, { Component } from "react";
import {
  Home,
  Login,
  Verify,
  AddItem,
  EditItem,
  DisplayItem,
  RequestReservation,
  EditGroup,
  InviteOwners,
  RequestSomething
} from "./components/screens";
import ConfiguredApollo from "./config/ConfiguredApollo";
import { BrowserRouter as Router, Route, Switch } from "react-router-dom";
import { IconContext } from "react-icons";
import { ToastContainer, toast } from "react-toastify";
import "react-toastify/dist/ReactToastify.css";

const makeToast = (message, color) => {
  const toastArgs = {
    position: "bottom-right",
    autoClose: 5000,
    hideProgressBar: true,
    closeOnClick: true,
    pauseOnHover: true,
    draggable: true
  };
  if (color === "red") {
    toast.error(message, toastArgs);
  } else {
    toast.success(message, toastArgs);
  }
};

const renderMergedProps = (component, ...rest) => {
  const finalProps = Object.assign({}, ...rest);
  return React.createElement(component, finalProps);
};

const PropsRoute = ({ component, ...rest }) => {
  return (
    <Route
      {...rest}
      render={routeProps => {
        return renderMergedProps(component, routeProps, rest);
      }}
    />
  );
};

const routing = (
  <Router>
    <div>
      <Switch>
        <PropsRoute exact path="/" component={Home} makeToast={makeToast} />
        <PropsRoute path="/login" component={Login} makeToast={makeToast} />
        <PropsRoute
          path="/verify/:email/:token?"
          component={Verify}
          makeToast={makeToast}
        />
        <PropsRoute
          path="/add-item/:groupId"
          component={AddItem}
          makeToast={makeToast}
        />
        <PropsRoute
          path="/edit-item/:itemId"
          component={EditItem}
          makeToast={makeToast}
        />
        <PropsRoute
          path="/item/:itemId"
          component={DisplayItem}
          makeToast={makeToast}
        />
        <PropsRoute
          path="/request-item/:itemId"
          component={RequestReservation}
          makeToast={makeToast}
        />
        <PropsRoute
          path="/edit-group/:groupId"
          component={EditGroup}
          makeToast={makeToast}
        />
        <PropsRoute
          path="/invite/:groupId"
          component={InviteOwners}
          makeToast={makeToast}
        />
        <PropsRoute
          path="/request"
          component={RequestSomething}
          makeToast={makeToast}
        />
        <PropsRoute component={Home} makeToast={makeToast} />
      </Switch>
    </div>
  </Router>
);

class App extends Component {
  render() {
    return (
      <ConfiguredApollo>
        <IconContext.Provider
          value={{ color: "blue", className: "react-icons" }}
        >
          {routing}
          <ToastContainer
            position="top-right"
            autoClose={5000}
            hideProgressBar
            newestOnTop={false}
            closeOnClick
            rtl={false}
            pauseOnVisibilityChange
            draggable
            pauseOnHover
          />
        </IconContext.Provider>
      </ConfiguredApollo>
    );
  }
}

export default App;
