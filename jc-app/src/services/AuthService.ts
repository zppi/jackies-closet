export const SESSION_TOKEN_KEY = "@AuthService/session_token";

let authLoaded = false;

let sessionToken: string | null = null;

let localStorage = window.localStorage;

export const signIn = (token: string): void => {
  localStorage.setItem(SESSION_TOKEN_KEY, token);
  sessionToken = token;
  authLoaded = true;
};

export const signOut = (): void => {
  localStorage.removeItem(SESSION_TOKEN_KEY);
  sessionToken = null;
};

export const getSessionToken = async (): Promise<typeof sessionToken> => {
  if (authLoaded) {
    return sessionToken;
  }

  sessionToken = await localStorage.getItem(SESSION_TOKEN_KEY);
  authLoaded = true;
  return sessionToken;
};

export const isSignedIn = async (): Promise<boolean> =>
  (await getSessionToken()) !== null;
