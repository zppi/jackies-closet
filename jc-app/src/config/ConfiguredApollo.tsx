import * as React from "react";
import { ApolloProvider } from "react-apollo";
import { ApolloClient } from "apollo-client";
import { setContext } from "apollo-link-context";
import { createHttpLink } from "apollo-link-http";
import { InMemoryCache } from "apollo-cache-inmemory";
import { SESSION_TOKEN_KEY } from "../services/AuthService";

interface State {
  client: ApolloClient<InMemoryCache> | null;
}

export default class ConfiguredApollo extends React.Component<any, State> {
  state: State = { client: null };
  async componentWillMount(): Promise<void> {
    let uri = "https://jackies-closet-server.herokuapp.com/graphql";

    if (!process.env.NODE_ENV || process.env.NODE_ENV === "development") {
      uri = "http://localhost:2700/graphql";
    }

    const httpLink = createHttpLink({
      uri,
      headers: {
        "Content-Type": "application/json"
      }
    });

    const authLink = setContext((_, { headers }) => {
      const token = localStorage.getItem(SESSION_TOKEN_KEY);
      // return the headers to the context so httpLink can read them
      return {
        headers: {
          ...headers,
          Authorization: token ? token : ""
        }
      };
    });
    const cache = new InMemoryCache();

    const client: ApolloClient<any> = new ApolloClient({
      link: authLink.concat(httpLink),
      cache
    });

    this.setState({ client });
  }

  render(): any {
    if (this.state.client === null) {
      return <div>Loading ...</div>;
    }

    return (
      <ApolloProvider client={this.state.client}>
        {this.props.children}
      </ApolloProvider>
    );
  }
}
